# List comprehension
print('# List comprehension')
# From normal forloop
print('\n# From normal forloop')
mystring = 'hello'
mylist = []

print('loop in mystring = "hello" and insert each letter into mylist = [] then print mylist: ')
for letter in mystring:
  mylist.append(letter)   # ['h', 'e', 'l', 'l', 'o']
print(mylist)


# Using list comprehernsion 
# *result*  = [*transform*    *iteration*         *filter*     ]
# List comprehension is an elegant way to define and create lists based on existing lists.
# List comprehension is generally more compact and faster than normal functions and loops for creating list.
# However, we should avoid writing very long list comprehensions in one line to ensure that code is user-friendly.

print('\n## Using list comprehernsion ex: 1')
print('mylist = [letter for letter in mystring]: ')
mylist = [letter for letter in mystring]
print(mylist)  # ['h', 'e', 'l', 'l', 'o']

print('\n## Using list comprehernsion ex: 2')
print("mylist = [x for x in [2,4,6,7,'lala']] : ")
mylist = [x for x in [2,4,6,7,'lala']]    
print(mylist)    # [2, 4, 6, 7, 'lala']

print('\n## Using list comprehernsion ex: 3')
print("mylist = [x for x in 'hello there'] : ")
mylist = [x for x in 'hello there']
print(mylist)  # ['h', 'e', 'l', 'l', 'o', ' ', 't', 'h', 'e', 'r', 'e']

print('\n## Using list comprehernsion ex: 4 using range(0,11)')
print("mylist = [num for num in range(0,11)] : ")
mylist = [num for num in range(0,11)]
print(mylist)

print('\n## Using list comprehernsion ex: 5 using range(0,11)')
print("mylist = [num**2 for num in range(0,11)] : ")
mylist = [num**2 for num in range(0,11)]
print(mylist)  # [0, 1, 4, 9, 16, 25, 36, 49, 64, 81, 100]


print('\n## Using list comprehernsion ex: 6 celcius calculation into a fahrenheit list value')
print("mylist = [num**2 for num in range(0,11)] : ")
celcius = [0,10,20,34.5]
fahrenheit = [(((9/5)*temp) + 32) for temp in celcius]
print(fahrenheit)  # [32.0, 50.0, 68.0, 94.1]

print('\n## Using list comprehernsion ex: 7 using normal for loop')
fahrenheit = []
for temp in celcius:
  fahrenheit.append((((9/5)*temp) + 32))
print(fahrenheit)  # [32.0, 50.0, 68.0, 94.1]



# Condition in list comprehension
print('\n# Condition in list comprehension')
print('# *result*  = [*transform*    *iteration*         *filter*     ]')

print('\n# If used at filter')
mylist = [x**2 for x in range(0,11) if x%2 == 0]
print(mylist)   # [0, 4, 16, 36, 64, 100]


print('\n# Nested if used at filter')
num_list = [y for y in range(100) if y % 2 == 0 if y % 5 == 0]
print(num_list)   # [0, 10, 20, 30, 40, 50, 60, 70, 80, 90]


# Condition in list comprehension
print('\n# If else use at transform')
results = [x if x%2 == 0 else 'ODD' for x in range(0,11)]
print(results)  # [0, 'ODD', 2, 'ODD', 4, 'ODD', 6, 'ODD', 8, 'ODD', 10]


# Condition example
print('\n# Condition example')
users = [{'name':'manuel','age':31,'email':'manuel@gmail.com'},{'name':'max','age':20,'email':'max@gmail.com'},{'name':'leta','age':31,'email':'leta@gmail.com'}]

user_name = [ user['name'] for user in users]
print(user_name)   # ['manuel', 'max', 'derrick']


emailOlder = [ user['email'] for user in users if user['age'] > 30 ]
print(emailOlder)   # ['manuel@gmail.com', 'derrick@gmail.com']



# Get specific line out from a text file using list comprehension and file system
print('\n# Get specific line out from a text file using list comprehension and file system')
f = open('15_list_file.txt','r')
result = [line for line in f if 'line3' in line]
print(result)
f.close()


# Nested loop
print('\n# Nested loop')
user_groups = [

  [{'name':'beka','age':31,'email':'beka@gmail.com','hobbies':['eating','boxing','cooking']},{'name':'buna','age':20,'email':'buna@gmail.com','hobbies':['cooking','running','reading']}],

  [{'name':'lezana','age':35,'email':'lezana@gmail.com','hobbies':['jogging','reading','swimming']},{'name':'kunda','age':11,'email':'kunda@gmail.com','hobbies':['baking','running','reading']}],
]

user_name = [person    for group in user_groups   for person in group    ]
print(user_name)

print('\n')

user_older_than_30 = [ person for group in user_groups  for person in group if person['age'] > 30  ]
print(user_older_than_30)

print('\n')

user_with_hobbies_name = [ person for group in user_groups for person in group  for hobby in person['hobbies'] if 'reading' in  hobby]
print(user_with_hobbies_name)
print('\n')


# Transpose of Matrix using Nested Loops
print('\n# Transpose of Matrix using Nested Loops ')
transposed = []
matrix = [[1,2,3,4],[5,6,7,8]]
for i in range(len(matrix[0])):
  transposed_row = []

  for row in matrix:
    transposed_row.append(row[i])
  transposed.append(transposed_row)

print(transposed)   # [[1, 5], [2, 6], [3, 7], [4, 8]]

matrix = [[1, 2], [3,4], [5,6], [7,8]]
transpose = [[row[i] for row in matrix] for i in range(2)]
print (transpose)   # [[1, 3, 5, 7], [2, 4, 6, 8]]
