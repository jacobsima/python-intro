# ******* Comparison operators

# Greater than
1 > 2

# less than
1 < 2

# Greater than or equal to
1 >= 2

# Less than or equal to
1 <= 2

# Equality  (python check for type, not like javascript where we need === for type and value checking)
1 == 1    # True
1 == '1'  # False

# Inequality
1 != 2


# *********** Logical Operator
# AND
(1 > 2) and (2 < 3)

# OR
(1 > 2) or (2 < 3)

#  Mutliple logical operators
(1 == 2) or (2 == 3) or (4 == 4)