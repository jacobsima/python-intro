# A Tuple is a collection which is ordered and unchangeable. Allows duplicate members.

# Create a tuple
fruits = ('Apples','Oranges',"Grapes")

# Use constructor
fruits2 = tuple(('Apples','Oranges','Mangos'))
print(fruits, fruits2)

# Signle value needs trailing comma
fruits3 = ('Apples', )  # this will return type of tuple
fruits4 = ('Apples')    # this will return type of string
print(type(fruits3))    # <class 'tuple'>
print(type(fruits4))    # <class 'str'>

# Get a value
print(fruits[1])   # Oranges

# Unchangeable  !
#fruits[0] = 'Pears'   # TypeError: 'tuple' object does not support item assignment

# Delete tuple
del fruits4
#print(fruits4)   #NameError: name 'fruits4' is not defined

# Get length
print(len(fruits))   # 3

t = ('a','b','c','d','a')
print(t.count('a'))  # 2

# t[0] = 'NEW'  # list can be re-assigned
#t[0] = 'NEW'   # TypeError: 'tuple' object does not support item assignment