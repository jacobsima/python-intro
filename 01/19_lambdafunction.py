# Simple function to convert to lambda func
print('\n# Simple function to convert to lambda func')
def getSum(a,b):
  total = a+b
  return total

sum = getSum(2,5)
print(sum)

#### A Lambda function is small anonymous function. same as Arrow function in javascript
print('\n# Basic lambda function')
getSum2 = lambda num1,num2: num1 + num2
print(getSum2(92,4))

# Map function
# map loop within each element of iteration and do the operation required within the function
print('\n# Map function')
def square(num):
  return num**2

myNum = [1,2,3,4,5]
# Iterate to it
for item in map(square,myNum):
  print(item)
"""
1
4
9
16
25
"""
print('\n')

lister = list(map(square,myNum))  # [1, 4, 9, 16, 25]
print(lister)


def splicer(mystr):
  return 'EVEN' if len(mystr)%2 == 0 else 'ODD'

names = ['andy','eve','sally']
listing = list(map(splicer,names))   # ['EVEN', 'ODD', 'ODD']
print(listing)


# Filter
# filter loop within each element of iteration and test them into the function
# if the test is true, the element is kept, if not the element is not include
# in the new list that filter return
print('\n# Filter')
def checkEven(num):
  return num%2 == 0

myNums = [1,2,3,4,5,6,7]
lister = list(filter(checkEven,myNums))
lister2 = list(map(checkEven,myNums))
print(lister)   # [2, 4, 6]
print(lister2)   # [False, True, False, True, False, True, False]

# Combine lambda function with map and filter

# With map
print('\n# Combine lambda function with map')
lister = list(map(lambda num: num**2, myNums))
print(lister)   # [1, 4, 9, 16, 25, 36, 49]

# with Filter
print('\n# Combine lambda function with filter')
filtered = list(filter(lambda num: num%2 == 0,myNums))
print(filtered)   # [2, 4, 6]
