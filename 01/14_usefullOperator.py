# Range Operator
print('\n# Range Operator')
mylist = [1,2,3,4,5,6,7,8,9,10]

for num in range(10):
  print(num)
"""
0
1
2
3
4
5
6
7
8
9
"""
print('\n# Specify a starting point in range')
for num in range(2,5):
  print(num)
'''
2
3
4
'''
# Add Skip
print('\n# Add Skip')
for num in range(3,15,3):
  print(num)
'''
3
6
9
12
'''

# Create a generator
listItem = list(range(0,11,2))
print(listItem)   # [0, 2, 4, 6, 8, 10]

# Enumerate 
print('\n# Enumerate ')
indexCount = 0
for letter in 'abcgrcad':
  print(f'At index {indexCount} the letter is {letter}')
  indexCount += 1

print('')
'''
At index 0 the letter is a
At index 1 the letter is b
At index 2 the letter is c
At index 3 the letter is g
At index 4 the letter is r
At index 5 the letter is c
At index 6 the letter is a
At index 7 the letter is d
'''

# Other way
indexCount = 0
word  = 'abcngdk'
for letter in word:
  print(f'At index {indexCount} the letter is {word[indexCount]}')
  indexCount += 1
print('\n')

# Use enumerate
# return a tuple of (index, item)
# enumarate() is a built in python function that adds a counter to an iterable
print('\n# Use enumerate return a tuple of (index, item) ')
for item in enumerate(word):
  print(item)
'''
  (0, 'a')
  (1, 'b')
  (2, 'c')
  (3, 'n')
  (4, 'g')
  (5, 'd')
  (6, 'k')

'''

for (index,item) in enumerate(word):
  print(f'index is {index} and trhe item is {item}')

print('\n')

# Zip Function
print('\n# Zip Function')

print('\# Zip Two list [1,2,3,4,5,6] and ["a","b","c"] ')
mylist1 = [1,2,3,4,5,6]
mylist2 = ['a','b','c']
listing = list(zip(mylist1,mylist2))     # [(1, 'a'), (2, 'b'), (3, 'c')]
print(listing)


print('\n# for loop in zip')
for item in zip(mylist1,mylist2):      # return tuple
  print(item)
'''
(1, 'a')
(2, 'b')
(3, 'c')
'''
print('\n')


print('\n# Working on ("Colman","Birman","harsing") and ("Dell","Apple","MS") ; zipped = zip(names,comps)')
names = ("Colman","Birman","harsing")
comps = ("Dell","Apple","MS")
zipped = zip(names,comps)
print(zipped)   # <zip object at 0x0069B7D8>
zippedList = list(zipped)
zippedSet = set(zip(names,comps)) 
zippedTuple = tuple(zip(names,comps))
zippedDict = dict(zip(names,comps))

print("list(zipped): ",zippedList)    # [('Colman', 'Dell'), ('Birman', 'Apple'), ('harsing', 'MS')]
print("set(zip(names,comps)) : ",zippedSet)     # {('Birman', 'Apple'), ('harsing', 'MS'), ('Colman', 'Dell')}
print("tuple(zip(names,comps)): ",zippedTuple)   # (('Colman', 'Dell'), ('Birman', 'Apple'), ('harsing', 'MS'))
print("dict(zip(names,comps)): ",zippedDict)   # zippedDict:  {'Colman': 'Dell', 'Birman': 'Apple', 'harsing': 'MS'}




# In Words
print('\n# In Words')
print('x' in [1,2,3])
print('x' in ['x',2,3])
print('x' in 'words of x')

print('\n')

# In in dict
print('\n# In in dict')
dictio = {'key1':1,'key2':2}
print('key1' in dictio)
print(1 in dictio.values())  # True
print(1 in dictio.keys())  # False

#  Accept User input
print('\n#  Accept User input')
result = input('What is your name: ')   # this input value has a type of string
print(f'Your name is {result}')


