# While loops execute a set of statements as long as a condition is true
print('\n# While Loop')
count  =  0
while count <=10:
  print(f'count: {count}')
  count +=1
"""
count: 0
count: 1
count: 2
count: 3
count: 4
count: 5
count: 6
count: 7
count: 8
count: 9
count: 10

"""

x = 0
while x < 5:
  if x < 5:
    print(f'The current value of x is {x}')
    x += 1
  else:
    print('x is not less than 5')
"""
The current value of x is 0
The current value of x is 1
The current value of x is 2
The current value of x is 3
The current value of x is 4
"""