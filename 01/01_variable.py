#  A Variable is a container for a value, which can be of various types
'''
This is a multiline comment or docstring (used to define a functions purpose) can be single or double quotes
'''
"""
Variables Rules:
  - Variable names are case sensitive (name and NAME are different variables)
  - Must start with a letter or an underscore
  - Can have numbers but can not start with one
"""

# Main Types
print('\n# Main Types')
x = 1           # int
y = 2.5         # float
name = 'Jacob'  # str
is_cool = True  # bool

# Multiple assignment
print('\n# Multiple assignment')
a,b,c,first_name,is_valid = (1,2,3,'luka',True)

# Output something
print('\n# Output something')
print('Hello')   # hello