# Arguments and Keywords Arguments
# Simple func with multiple argument
print('\n# Simple func with multiple argument')
def myfunc(a,b,c=0,d=0,e=0):
  return sum((a,b,c,d,e))*0.05
result = myfunc(40,60,20,120,2)
print(result)
# if arg are more than 5 we get an error
#TypeError: myfunc() takes from 2 to 5 positional arguments but 6 were given
# result = myfunc(40,60,100,200,300, 400)


# Using *args in as function argments
# treat this as a tuple of parameters that are coming in
# You can add as much as arguments you want
# the word agrs is arbitrairy
print('\n# Using *args in as function argments')
def myfunc2(*args):
  print(args)      # (40, 60, 100)  *args is a list of all the arguments
  for arg in args:
    print(arg) 
  return sum(args)*0.05  

result = myfunc2(40,60,100)
print(result)

# Using *kwargs in as function argments
# Build a keyword arguments
# the kwargs is a dictionary
def myfunc3(**kwagrs):
  print(kwagrs)    # {'fruit': 'apple','veggie' = 'lettuce'}
  if 'fruit' in kwagrs:
    value = kwagrs['fruit']
    print(f'My fruit of choice is {value}')
  else:
    print('I did not find any fruit')
myfunc3(fruit='apple',veggie='lecture')



#  Use both in combinaison
# the only requirement here , it is to respect the order of arguments, first we have args, then kwargs
# You can put as much as args or kwargs you want.
print('\n#  Use both in combinaison')
def myfunc4(*args,**kwargs):
  print('I would like {} {}'.format(args[0],kwargs['food']))
myfunc4(20,89,29,3,2,fruit='orange',food='eggs')