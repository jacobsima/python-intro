# boolean has only two value
# True or False

print(True)         # True
print(False)        # False

print(type(True))   # <class 'bool'>
print(type(False))  # <class 'bool'>

print( 1 > 3)       # False
print(1 == '1')     # False
print(1 == 1)       # True

b = None
print(b)           # None