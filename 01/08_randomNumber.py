# Random Number
# Python does not have a random() function to make a random number, but Python has a built-in module called random that can be used to make random numbers:
print('\n# Random Number ')
# import random
from random import randrange,randint,choice,shuffle,sample,random,uniform

#*** Functions for integers
print('\n#*** Functions for integers')
print('randrange(10) ')
x = randrange(10)     # random.randrange(stop): any integer from 0-9
print(x)
print('randrange(10,20,1)')
y = randrange(10,20,1) # random.randrange(start, stop, step)
print(y)
print('randint(30,40)')
z = randint(30,40)
print(z)


#*** Functions for sequences
print('\n#*** Functions for sequences')

#****** Choice
# random.choice: Return a random element from the non-empty sequence seq. If seq is empty, raises IndexError.
print("***** Choice")
x = ["Lena",'Luna','Latifa','Bena',"kunga",{'name':'jacob','age':31},True,None]
y = []
print(choice(x))
# print(choice(y))  # IndexError: Cannot choose from an empty sequence


#****** Shuffle
#The shuffle() method takes a sequence (list, string, or tuple) and reorganize the order of the items.
print("***** Shuffle")
fruits =  ["orange",'banana','apple','mangos',"avocados",'bingo']
print('fruits:',fruits)
shuffle(fruits)
print('fruits:',fruits)

#****** Sample
"""
The random.sample() function has two arguments, and both are required.

The population can be any sequence such as list, set from which you want to select a k length number.
The k is the number of random items you want to select from the sequence.  k must be less than the size of the specified list.
A random.sample() function raises a type error if you miss any of the required arguments.
"""
print('\n#****** Sample')
print(sample(fruits,3))



#*** Real-valued distributions
print('\n#*** Real-valued distributions')

#****** Random
# Return the next random floating point number in the range [0.0, 1.0).
print('\n****** Random')
print(random())

#****** Uniform
# Return a random floating point number N 
print('\n****** Uniform')
print(uniform(1,8))

