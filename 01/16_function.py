#### A Function is a block of code which only runs when it is called. In pyhton, we do not use curly brackets, we use indentation with tabs or spaces

# Create a function
print('\n# Create a function')
def sayHello(name):
  print(f'Hello {name}')

sayHello('Jacob')

# Default argument value
print('\n# Default argument value')
def callMe(name='Blanche'):
  print(f'you can call me {name}')

callMe()

# Return value
print('\n# Return value')
def getSum(a,b):
  total = a+b
  return total

sum = getSum(2,4)
print(sum)

def addNum(num1,num2):
  if type(num2) ==  type(num1) == type(10):
    return num1 + num2
  else:
    return 'Sorry wrong arguments values'

result = addNum(2,3)
# print(type(result))
print(result)
