# problem : Find out if the word dog in a string
print('\n# problem : Find out if the word dog in a string')
def findOut(str):
  if 'dog' in str.lower():
    return True
  else:
    return False

result = findOut('Hello bana mboka doggy')   
print(result)   # True

# This is a clean code compare to the top code
def checkOut(str):
  return 'dog' in str.lower()
result = checkOut('The red one running, yes that my dog')
print(result)  # True

# ******** PIG LATIN
'''
if word starts with a vowel, add 'ay' to end
if word does not start with vowel, put first letter at the end, then add 'ay'
eg: word ----> ordway
eg: apple ----> appleay
'''
print('\n# ******** PIG LATIN')
voyel = ['a','e','i','o','u','y']
def pigLatin(word):
  startWord = word[0]
  if startWord in voyel:
    return word +'ay'
  else:
    return word[1:]+startWord+'ay'

result = pigLatin('word')
print(result)

print('\n# Using ternary operator')
def getPig(word):
  startWord = word[0]
  return word + 'ay' if startWord in voyel else word[1:]+startWord +'ay'

result = getPig('apple')
print(result)

    