# If/Else conditions are used to decided to do something based on something being true or false

x = 3
y = 25

### Comparison Operators (==, !=, >, <, >=, <=) - used to compare values

## Simple if
if x > y:
  print(f'{x} is greather than {y}')

## if/else

if x > y:
  print(f'{x} is greather than {y}')
else:
  print(f'{y} is greater than {x}')

## elif
if x > y:
  print(f'{x} is greater than {y}')
elif x == y:
  print(f'{x} is equal to {y}')
else:
  print(f'{y} is greather than {x}')

# Nested if
if x > 2:
  if x <= 10:
    print(f'{x} is greater than 2 and less than or equal to 10')


# Logical Operators (and, or, not) - used to combine conditional statements
# And
if (x > 2 and x <= 10):
  print(f'{x} is greater than 2 and less than or equal to 10')

# Or
if(x > 2 or x <=10 ):
  print(f'{x} is greater than 2 and less than or equal to 10')

# not
if not(x == y):
  print(f'{x} is not equal to {y}')

### Membership Operators (in, not in) - Membership operators are used to test if sequence is presented in an object
numbers = [1,2,3,4,5,6]
# In
if x in numbers:
  print(x in numbers)    # True

# not in

if x not in numbers:      # False and it not gonna print or do anything
  print(x not in numbers)

z = 12
if z not in numbers:      # True : then it is called
  print(z not in numbers)

### Identity Operators (is, is not) - Compare the object, not if they are equal, but if they are actually the same object, with the same memory location

# Is
if x is y:
  print(x is y)

# is not
if x is not y:      # True
  print(x is not y)