# A Set is a collection which unordered and unidexed. No duplicate members.
# Sets are unordered collection of unique element
# like a ditc but without values only keys

# Create a set
fruit = {'luna','kuba','congo','luna'}   # {'congo', 'kuba', 'luna'} does not except 2 or more same item
print(fruit)

# Check if set
print('luna' in fruit)    # True

# Add to a set at the beginning of a set
fruit.add('bana')
print(fruit)    # {'bana', 'congo', 'luna', 'kuba'}


# Remove from set
fruit.remove('luna')
print(fruit)

# Clear set
fruit.clear()
print(fruit)

del fruit
# print(fruit) # NameError: name 'fruit' is not defined

x =  set()
x.add(True)
x.add(2)
x.add(1)
x.add('z')
print('x is : ',x)

k = {1,2,4,True}
converted = set([1,1,1,1,2,2,2,2,3,3,3])
print(converted)   # {1, 2, 3}



