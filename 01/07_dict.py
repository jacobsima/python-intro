# A Dictionary is a collection which is unordered, changeable and indexed. No duplicate members.

# Create a dict
person ={
  'first_name' : 'Jacob',
  'last_name': 'Sima',
  'age': 30,
}
print(person, type(person))
# {'first_name': 'Jacob', 'last_name': 'Sima', 'age': 30} <class 'dict'>

# Use constructor
person2 = dict(first_name='lenzy',last_name='willem')
print(person2)  # {'first_name': 'lenzy', 'last_name': 'willem'}

# Get a value
print(person['first_name'])  # Jacob
print(person['last_name'])   # Sima

# Add key/value
person['phone'] = '555-555-3434'
print(person)  # {'first_name': 'Jacob', 'last_name': 'Sima', 'age': 30, 'phone': '555-555-3434'}


# Get dict items => return an iterable of dict items as tuple 
print(person.items())  
# dict_items([('first_name', 'Jacob'), ('last_name', 'Sima'), ('age', 30), ('phone', '555-555-3434')])
for i in person.items():
  print(i)
  print(type(i))
"""
('last_name', 'Sima')
<class 'tuple'>
('age', 30)
<class 'tuple'>
('phone', '555-555-3434')
<class 'tuple'>
"""


# Get dict keys => return an iterable of dict keys
print(person.keys())  # dict_keys(['first_name', 'last_name', 'age', 'phone'])
print(type(person.keys()))  # <class 'dict_keys'>

for t in person.keys():
  print(t)
"""
first_name
last_name
age
phone
"""

# get dict values => return an iterable of values
print(person.values())   # dict_values(['Jacob', 'Sima', 30, '555-555-3434'])
for v in person.values():
  print(v)
"""
Jacob
Sima
30
555-555-3434
"""

# Copy a dict
person3 = person.copy()
print(person)
print(person3)

# Remove Item
del(person3['age'])
print(person3)
person3.pop('phone')
print(person3)

# Clear dict
person3.clear()
print(person3)

# Get length
print(len(person))   # 4


# Get from index
people = [
  {'name':'Martha', 'age':30},
  {'name':'Sarah', 'age':20},
  {'name':'Jhon', 'age':52}

]

print(people)
print(people[1])

# Redefine a dict
people[0]['name']="Gombe"
print(people)

# In a dict
print('\n# in a dict')
print('first_name' in person)  # True
