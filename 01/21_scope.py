"""
LEGB Rule:

  * L : Local --- Names assigned in any way within a function(def or lambda),and not declared global in that function.

  * E : Enclosing function locals --- Names in the local scope of any and all enclosing functions(def or lambda), from inner to outer.

  * G : Global(module) --- Names assigned at the top-level of a module file, or declared global in a def within the file.

  * B : Built-in(Python) --- Names preassigned in the built-in names module: open, range, SyntaxError,...
"""
# Local variable
print('\n# Local variable')
lambda num: num**2    # this num a local on this lambda function

name = 'This is a global string'   # Global scope
def greet():
  # Enclosing scope
  name = 'Sammy'  
  def hello():
    # Local scope
    name = 'I am Local'
    print('hello '+name)  
  hello()     
greet()


x = 25
def my_func():
  x = 50
  return x

def my_func2():
  return x

print(x)   # 25
print(my_func()) # 50
print(my_func2()) # 25

print('\n# Change global variable')
# What if I wanna change the global value
y = 50
def funcz():
  global y
  print(f'y is {y}')

  # local reassigment on a global variable
  y = 300
  print(f'global variable y changed to {y}')

funcz()
print(y)



# in If statement and global
print('\# in If statement and global')
x = 50
def exing():
  x = 30
  print(x)

  if x == 30:
    x = 12
    print(x)
  
  print(x)
exing()


print(x)



