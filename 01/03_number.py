# There are three numeric types in Python
# int
# float
# complex

# Numeric types
print('\n# Numeric types')
x = 1     # int
y = 2.8   # float
z = 2+3j  # complex

print(x, type(x))
print(y, type(y))
print(z, type(z))

# int
# Int, or integer, is a whole number, positive or negative, without decimals, of unlimited length.
print('\n*** Integer')
x = 1
y = 35656222554887711
z = -3255522

print(x,type(x))
print(y,type(y))
print(z,type(z))

# Float
# Float, or "floating point number" is a number, positive or negative, containing one or more decimals.
# Float can also be scientific numbers with an "e" to indicate the power of 10.
print('\n# Float')
x = 1.10
y = 1.0
z = -35.68
k = 35e3
l = 12E4
m = -87.7e100

print(x,type(x))
print(y,type(y))
print(z,type(z))
print(k,type(k))
print(l,type(l))
print(m,type(m))


# Complex
# Complex numbers are written with a "j" as the imaginary part:
print('\n# Complex')
x = 3 + 5j
y = 5j
z = -5j

print(x, type(x))
print(y, type(y))
print(z, type(z))


# Type Conversion
print("\n# Type Conversion")
x = 1
y = 2.8
z = 1j
print('x:',x)
print('y:',y)
print('z:',z)
print('\n*** convert from int to float:')
a = float(x)
print('a = float(x): ',a)

print('\n*** convert from float to int:')
b = int(y)
print('b = int(y): ',b)

print('\n*** convert from int to complex:')
c = complex(x)
d = complex(x,y)
print('c = int(x): ',c)
print('d = complex(x,y): ',d)




# Number methods
print("\n# Number methods")
from math import ceil,floor

x = 12.9
y = 9.3
z = -23
a = 4
print("x = :",x)
print("y = :",y)
print("z = :",z)
print("a = :",a)

print('\n*** abs:')
print('abs(-23)= ',abs(z))

print('\n*** ceil:')
print('ceil(9.3)= ',ceil(y))

print('\n*** floor:')
print('floor(12.9)= ',floor(x))

print('*** min/max:')
mylist = [10,20,30,40,50,100]
print(min(mylist))
print(max(mylist))


print('\n')
print('min(2,938,3,9) = ',min(2,938,3,9))
print('max(39,28,329,983) = ',max(39,28,329,983))
print('sum([2,9,0,3,8]) = ',sum([2,9,0,3,8]))
print('round(3.8382,2) =',round(3.8382,2))
print('round(3.2) = ',round(3.2))
print('2**10 = ',2**10)
print(pow(2,10))
print(len('Mathematics'))















