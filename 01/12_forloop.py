# A for loop is used for iterating over a sequence (that is either a list, a tuple, a dictionary, a set, or a string).

# Iterate in a LIST
print('\n# Iterate in a LIST')
mylist = [1,2,3,4,5,6,7,8,9,10]

for num in mylist:
  print(num)

print('\n')

for num in mylist:
  print(f'Hello number {num}')

print('\n')

for num in mylist:
  if num % 2 == 0:
    print(f'{num} is an even')
  else:
    print(f'{num} is an odd')

# Do iteration Sum
print('\n# Do iteration Sum')
listSum = 0
for num in mylist:
  listSum += num
  print(listSum)



# Iterate in a string
print('\n# Iterate in a string')
mystr = 'hello there'
for letter in mystr:
  print(letter)

# Iterate in tuple
print('\n# Iterate in tuple')
tup = (1,2,3)
for item in tup:
  print(item)

# Tuple unpacking
print('\n# Tuple unpacking')
mylistTup = [(1,2),(3,4),(5,6),(7,8)]
print(len(mylistTup))
print('\n')

for item in mylistTup:
  print(item)
'''
(1, 2)
(3, 4)
(5, 6)
(7, 8)
'''
print('\n')
for (a,b) in mylistTup:
  print(a)
  print(b)
"""
1
2
3
4
5
6
7
8
"""

print('\n')
mylistTup2 = [(1,2,3),(4,5,6),(7,8,9)]
for (a,b,c) in mylistTup2:
  print(a,b,c)
"""
1 2 3
4 5 6
7 8 9
"""
# Iterate in a Dict
print('\n# Iterate in a Dict')
person ={
  'first_name' : 'Jacob',
  'last_name': 'Sima',
  'age': 30,
}

# Simple iteration output only the keys
for p in person:
  print(p)
"""
first_name
last_name
age
"""
print('\n')

# Iteration output keys values pairs as tuple
for item in person.items():
  print(item)
"""
('first_name', 'Jacob')
('last_name', 'Sima')
('age', 30)
"""
print('\n')

for (keys,values) in person.items():
  print(values)
"""
Jacob
Sima
30
"""

print('\n')
# Get values
for value in person.values():
  print(value)


# Custom range
print('\n# Custom range')
rang = list(range(5))
print(rang)  # [0, 1, 2, 3, 4]

x = list(range(1,10))
print(x)    # [1, 2, 3, 4, 5, 6, 7, 8, 9]


print(range(0,11))
for i in range(0,11):
  print(f'number is: {i}')
"""
number is: 0
number is: 1
number is: 2
number is: 3
number is: 4
number is: 5
number is: 6
number is: 7
number is: 8
number is: 9
number is: 10
"""


# Loop Keywaords break, continue, pass

# Pass
x = [1,2,3]
for item in x:
  #comment     # only this gives an error, you can add pass to do nothing at all without error
  # You keep this as a place holder for any plan execution
  pass



# Continue
print('')
mystring = 'Sammy mokili mbanga ya taxa kolo'
for letter in mystring:
  if letter == 'a':
    continue
  if letter == 'x':
    break
  print(letter)