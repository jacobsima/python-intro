# A list is a collection which is ordered and changeable. Allows duplicate members

# Create a list
numbers = [1,2,3,49,4,3]
fruits = ['Apples','Oranges','Grapes','Pears']

# Use a constructor
numbers2 = list((1,2,3,4,5,'lena'))
print(numbers, numbers2)

# Get a value
print(fruits[1])

# Get lentgh
print(len(fruits))

# Append to a list : add at the end of the list
fruits.append('Mango')
print(fruits)

# Change a value
fruits[0] = "Blueberries"

# Remove from list : an item
fruits.remove('Grapes')
print(fruits)

# Remove at certain position
fruits.pop(2)
print(fruits)

# Remove at the end
fruits.pop()
print(fruits)

# Insert into a position
fruits.insert(2,"nguba")
print(fruits)

# Reverse the list
print(numbers)
numbers.reverse()
print(numbers)

# Sort
numbers.sort()
print(numbers)

# Reverse Sort list
print(fruits)
fruits.sort(reverse=True)
print(fruits)


# Index and Slicing
mylist = [1,2,3,4,5,6,7,8]
mylist2 = ['hello',True,293,[82,21],False,None]
print(mylist[0])          # 1
print(mylist[-1])         # 8
print(mylist[-3])         # 6
print(mylist[:3])         # [1, 2, 3] from 0 index to 3 but not include 3

# Merge list
mylist.extend(mylist2)
print(mylist)

print(mylist[-3])       # 
a = ['spam', 'egg', 'bacon', 'tomato', 'ham', 'lobster']
print('count:',a.count('spam'))
print(a)
a.clear()
print(a)

# Empty a list



# List Comprehension
matrix = [[1,2,3],[4,5,6],[7,8,9]]
print(matrix[0][0])   # 1

first_col = [row[0] for row in matrix]
print(first_col)  # [1, 4, 7]

