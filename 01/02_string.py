# String in python are surrounded by either single or double quotation marks. Let 's look at string formatting and some methods

# basic
print('\n# String assignment')
'hello'
"hello"

a = " I'm a student"
b = 'I\'m a student'
age = 27

print(a)
print(b)


# Checking String Type
print('\n# Checking String Type')
print(type(a))      # <class 'str'>
print(type(b))      # <class 'str'>
print(type(age))    # <class 'int'>

# Casting string
x = 1         # int
y = 2.5       # float
name = "john" # str

print('\n# Casting a String')
x = str(x)
y = str(y)
z =str(name)

print(type(x))    #<class 'str'>
print(type(y))    #<class 'str'>
print(type(z))    #<class 'str'>

# String Formatting
print('\n# String formatting')
print('Hello, my name is ' + name + 'and I am' + str(age))
print("My name is {name} and I am {age}".format(name=name,age=age))
print(f'Hello, my name is {name}  and I am {age}')
print('The text was insert here: "{}" by using .format(\'INSERT ME\')'.format("INSERT ME"))


# Get string value
# index = 0 when started from start of the string
# index = -1 when started from end of string
# IndexError : if index provided is out of range
print('\n# Get String Character at given string index')
mystring = 'hello there bana mayi, boni kuna'
print('string to read character is: ',mystring)
print('mystring[0]: ',mystring[0])    # mystring[0]:  h
print('mystring[4]: ',mystring[4])    # mystring[4]:  o
print('mystring[30]: ',mystring[30])  # mystring[30]:  n
print('mystring[31]: ',mystring[31])  # mystring[31]:  a
# print('mystring[32]: ',mystring[32])  # IndexError: string index out of range
print('mystring[-1]: ',mystring[-1])  # mystring[-1]:  a
print('mystring[-11]: ',mystring[-11])  # mystring[-1]:  a


#Immutability
# string is not immutable
# TypeError is generated in case of string immutation
print('\n# Immutability')
name = "Sammy"
print('name is:',name)
# name[0] = "P"  # TypeError: 'str' object does not support item assignment
print('name[0] = "P" => TypeError: \'str\' object does not support item assignment')
print('instead we do : "lastletter = name[1:] then  name = \'P\' + lastletter" ')
lastletter = name[1:]
name = 'P' + lastletter
print(name)

# String length and String Slice
print('\n# String length and String Slice')
stringOne =  'I am working as a developer at AvBot coperation'
print('string is: ', stringOne)
print('stringOne length is: ', len(stringOne))  # 47
print('stringOne[]:',stringOne)         # I am working as a developer at AvBot coperation
print('stringOne[:]:',stringOne[:])     # get everything
print('stringOne[::]:',stringOne[::])    # get everything
print('stringOne[2:]:',stringOne[2:])   # start from index 2 to all the way to the end
print('stringOne[:20]:',stringOne[:20])  # start from 0 to index 20 but not include index 20
print('stringOne[5:20]:',stringOne[5:20]) # start from 5 to index 20 but not include index 20
print('stringOne[::2]:',stringOne[::2])  # get everything but step by 2


# Time a letter
print('\n# Time a letter')
letter = 'z'
print("letter = 'z'*10: ",letter*10)



# String Methods
print('\n# String Methods')
l = 'hello there'
m = 'the new year eve'
n = 'hellothere'

print("l:",l)
print("m:",m)
print("n:",n)

# Capitilize string
print('\n*** Capitilize string')
print('l.capitalize(): ',l.capitalize(),'\n')

# Upper all case
print('*** Upper all case')
print('l.upper(): ',l.upper(),'\n')

# lower all case
print('*** Lower all case')
print('l.lower(): ',l.lower(),'\n')

# Swap case
print('*** Swap case')
print('m.swapcase(): ',m.swapcase(),'\n')

# Replace
print('*** Replace')
print('m.replace("new","old"): ',m.replace('new','old'),'\n')

# Count
print('*** count')
print('n.count("l"): ',n.count('l'),'\n')

# Starts with
print('\n*** Starts with')
print("n.startswith('h')",n.startswith('h'),'\n')   # True

# Ends with
print('\n*** Ends with')
print("n.endswith('t')",n.endswith('t'),'\n')   # False


# Find Position
print('\n*** Find Position')
print("n.find('t')",n.find('t'),'\n')   # 5
print("n.find('x')",n.find('x'),'\n')   # -1


# Is all alphabetic
print('\n*** Is all alphabetic')
print("n.isalpha()",n.isalpha(),'\n')   # True


# Is all alphanumeric
print('\n*** Is all alphanumeric')
print("n.isalnum()",n.isalnum(),'\n')   # True


# is all numeric
print('\n*** is all numeric')
print("n.isnumeric()",n.isnumeric(),'\n')   # False

# Split a string
print('\n*** Split a string')
print('m.split(""):',m.split(" "),'\n')  # ['the', 'new', 'year', 'eve']





 
