#  Javascript Object Notation
import json
# JSON to DICT CONVERSION IN PYTHON
print('\n# JSON to DICT CONVERSION IN PYTHON')
# object                    dict
# array                     list
# string                    str
# number(int)               int
# number(real)              float
# true                      True
# false                     False
# null                      None
people_string = """

{
  "people":[
    {
      "name":"John Snith",
      "phone":"555-555-5555",
      "emails":["john@gmail.com","jsmith@gmail.com"],
      "has_licenced":false
    },
    {
      "name":"Jane Doe",
      "phone":"333-333-2082",
      "emails":null,
      "has_licenced":true
    }
  ]

}

"""
# Convert JSON data into PYTHON DICT
print('\n# Convert JSON data into PYTHON DICT')
data = json.loads(people_string)    # this load a string
print(data)
# {'people': [{'name': 'John Snith', 'phone': '555-555-5555', 'emails': ['john@gmail.com', 'jsmith@gmail.com'], 'has_licenced': False}, {'name': 'Jane Doe', 'phone': '333-333-2082', 'emails': None, 'has_licenced': True}]}

# Type of Converted data
print('\n# Type of Converted data')
print(type(data))    # <class 'dict'>

# data people key
print('\n# data people key')
print(data['people'])
# [{'name': 'John Snith', 'phone': '555-555-5555', 'emails': ['john@gmail.com', 'jsmith@gmail.com'], 'has_licenced': False}, {'name': 'Jane Doe', 'phone': '333-333-2082', 'emails': None, 'has_licenced': True}]



# Loop in people
print('\n# Loop in people')
for person in data['people']:
  print(person)


# DICT to JSON CONVERSION IN PYTHON
print('\n# DICT to JSON CONVERSION IN PYTHON')
new_string = json.dumps(data,indent=2,sort_keys=True)
print(new_string)




# LOAD A JSON FILE INTO PYTHON
print('\n# LOAD A JSON FILE INTO PYTHON')

with open('states.json') as f:
  data = json.load(f)
  

for state in data['states']:
  print(state)




# CREATE  A JSON DATA FILE
print('\n# CREATE  A JSON DATA FILE')
print('\n')

with open('new_state.json','w') as f:
  json.dump(data,f,indent=2)




# Make a httrequest to get json response on the net
print('\n# Make a httrequest to get json response on the net')
print('\n')
import json
from urllib.request import urlopen

# You can also use the request library
with urlopen("https://jsonplaceholder.typicode.com/users") as response:
  source = response.read()

# convert string json data into a python dict
data = json.loads(source)

# convert it back into json data with nice indent
data2 = json.dumps(data,indent=2)
print('\n')
print(data2)