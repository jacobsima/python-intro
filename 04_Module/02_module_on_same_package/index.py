# Standard library
import random

# Custom module
from finding import indexes as finder,test  # module in same directory as called module
import validator 
import pig_latin_game_to_play as game  # Use Alias name


courses = ['History','Math','Physics','CompSci']



# From standard library
print('\n# From standard library')
random_course = random.choice(courses)
print(random_course)


# From indexes module
print('\n# From indexes module')
result = finder(courses,'CompSci')
print(result)
print(test)



# From validatorEmail module
print('\n# From validatorEmail module')
email = 'testgmail.com'

if(validator.validatorEmail(email)):
  print('Email is valid')
else:
  print('Email is not valid')



# From pig_latin_game_to_play module
print('\n# From pig_latin_game_to_play module')
result = game.pglatin('apple')
print(result)



# Sys.path
print('\n# Sys.path')
import sys
print(sys.path)

# order of search the right path of module to be imported, this is searched in sys.path array
# 0 : is the directory containing the script that we are running
# 1 : directories listed in python path environement variable
# 2 : Standard python library or core library
# 3 : Site-packages for third party library
'''
['c:\\Jacob\\python\\python-intro\\04_Module\\01_Custom_Module_OnSame_Package', 'C:\\Users\\Jacob\\AppData\\Local\\Programs\\Python\\Python37-32\\python37.zip', 'C:\\Users\\Jacob\\AppData\\Local\\Programs\\Python\\Python37-32\\DLLs', 'C:\\Users\\Jacob\\AppData\\Local\\Programs\\Python\\Python37-32\\lib', 'C:\\Users\\Jacob\\AppData\\Local\\Programs\\Python\\Python37-32', 'C:\\Users\\Jacob\\AppData\\Roaming\\Python\\Python37\\site-packages', 'C:\\Users\\Jacob\\AppData\\Roaming\\Python\\Python37\\site-packages\\win32', 'C:\\Users\\Jacob\\AppData\\Roaming\\Python\\Python37\\site-packages\\win32\\lib', 'C:\\Users\\Jacob\\AppData\\Roaming\\Python\\Python37\\site-packages\\Pythonwin', 'C:\\Users\\Jacob\\AppData\\Local\\Programs\\Python\\Python37-32\\lib\\site-packages']


'''



