import os
from datetime import datetime



# Get current directory
print('\n# Get current directory')
print(os.getcwd())    # C:\Jacob\python\python-intro


# Change working directory
print('\n# Change working directory')
os.chdir('/Users/Jacob/Documents/My Received Files/')
print('New working directory-- ', os.getcwd())     # C:\Users\Jacob\Documents\My Received Files




# List all the file in this directory
print('\n# List all the file in this directory')
print(os.listdir())
'''
  ['dittoJobs.mp4', 'Manage Microsoft Office Trusted Locations', 'react.txt', 'Screenshot from 2019-11-26 10-36-30.png', 'Wiz Khalifa - Something New feat. Ty Dolla $ign [Official Music Video].mp3']
'''



# Get each file size using: os.path.getsize()
print('\n# Get each file size using: os.path.getsize()')
files = os.listdir()
for file in files:
  size = os.path.getsize(file)
  print(f'{file}----{size}----bytes')



# Look at os.path.join()
# use many paths
print('\n# Look at os.path.join()')
file_path = os.path.join(os.getcwd(),'Manage Microsoft Office Trusted Locations','python-script.txt') 
print(file_path)



# Create the file
print('\n# Create the file')
with open(file_path,'w') as f:
  f.write('Document Writen from python script')
  print('Document created at: ',file_path)



# Check if file exists
print('\n# Check if file exists')
print(os.path.exists(file_path))      # True
print(os.path.exists('reader.txt'))   # False


# Check if directory of file
print('\n# Check if directory of file')
print(os.path.isdir(file_path))   # False
print(os.path.isfile(file_path))  # True


# Get the directory name of the file
print('\n# Get the directory name of the file')
print(os.path.dirname(file_path))




# Get the file basename
# python-script.txt
print('\n# Get the file basename')
print(os.path.basename(file_path))




# Print absolute path of the file
print('\n# Get absolute path of the file')
print(os.path.abspath(file_path))




# Split the extension from the pathname
# return a tuple of: dirname,basename
print('\n# Split the abslute pathname using os.path.split(), return tuple of (dirname,basename)')
print(os.path.split(file_path))

"""
('C:\\Users\\Jacob\\Documents\\My Received Files\\Manage Microsoft Office Trusted Locations', 'python-script.txt')

"""




# Split the abslute pathname using os.path.splitext(), 
# return tuple of (file directory and filename,file extension
print('\n# Split the abslute pathname using os.path.splitext(), return tuple of (file directory and filename,file extension)')
print(os.path.splitext(file_path))
'''
('C:\\Users\\Jacob\\Documents\\My Received Files\\Manage Microsoft Office Trusted Locations\\python-script', '.txt')
'''




# Get last time file access
print('\n# Get last time file access')
my_time = os.path.getatime(file_path)
print(datetime.fromtimestamp(my_time))

