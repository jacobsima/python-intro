# A Module is basically a file containing a set of functions to include in your application.
# there are core python modules, modules you can install using the pip package manager (including Django) as well as custom modules
# Module is python file ending with in.py  while package is a collection of modules in a folder



# Standard or core library imports
# eg: import datetime
# eg: from time import time



# Third party imports
# eg: from camelcase import CamelCase
# eg: import pandas as pd



# import custom module







