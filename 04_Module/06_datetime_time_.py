import datetime

# 1. Working with datetime.date
print("\n# Working with datetime.date")
print('datetime.date works only with : year,month,day')
# Create a date
print('# Create a date')
d = datetime.date(2016,7,24)  # pass regular int with no-leading zero
print(d)

# Get today date
print('\n# Get the today date')
tday = datetime.date.today()
print(tday)   
print(tday.year)
print(tday.day)
print(tday.month)

# Get today weekday
# weekday => monday = 0 and sunday = 6
# isoweekday => monday = 1 and sunday = 7
print('\n# Get today weekday')
print(tday.weekday())
print(tday.isoweekday())

# Time delta, difference between 2 days per time
# Print the current date from 10days from now 
print('\n# Print the current date from 7days from now ')
tdelta = datetime.timedelta(days=10)
print(tday + tdelta)

print('\n# Print what will be the date a week from now ')
tdelta = datetime.timedelta(days=7)
print(tday + tdelta)

print('\n# Print what was the date one week ago')
print(tday - tdelta)

# How many days until my birthday
print('\n# How many days until my birthday')
# date2 = date1 + timedelta
# timedelta = date1 + date2
bday = datetime.date(2020,7,8)
till_bday = bday - tday
print(till_bday)    # 37 days, 0:00:00
print(till_bday.days)    # 37 days, 0:00:00



# 2. Working with datetime.time
print("\n# Working with datetime.time")
print('datetime.time works only with : hours,minutes,secondes,microsecondes')

# Create new time
print('\n# Create new time')
t = datetime.time(9,30,45,100000)
print(t)   # 09:30:45.100000
print(t.hour)     # 9
print(t.minute)   # 30
print(t.second)   # 45
print(t.microsecond)   # 100000



# 3. Working with datetime.datetime
print("\n# Working with datetime.datetime")
print('# datetime.datetime gives access to everything')

# Create datetime
dt = datetime.datetime(2016,7,26,12,30,45,100000)
print(dt)
print(dt.date())   # 2016-07-26
print(dt.time())   # 12:30:45.100000
print(dt.year)     # 2016
print(dt.hour)     # 12

print("\n# Print 5 days into the future")
tdelta  = datetime.timedelta(days=5)
print(dt + tdelta)

print("\n# Print 9 hours into the future")
tdelta  = datetime.timedelta(hours=9)
print(dt + tdelta)

# Other constructor from datetime.datetime
print('\n# Other constructor from datetime.datetime')
dt_today = datetime.datetime.today()     # today date with timezone of none
dt_now = datetime.datetime.now()         # has an option to pass a timezone
dt_utcnow = datetime.datetime.utcnow()

print(dt_today)    
print(dt_now)
print(dt_utcnow)






