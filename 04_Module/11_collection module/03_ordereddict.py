from collections import OrderedDict
# Regular dict
print('\n# Regular dict')
d = {}
d['a'] = 1
d['b'] = 'hello'
d['c'] = 'lemba yasa'
d['d'] = 34
for k,v in d.items():
  print(k,v)    # the order is mixed up


# Using orderedict
print('\n# Using orderedict')
d = OrderedDict()
d['a'] = 1
d['b'] = 'hello'
d['c'] = 'lemba yasa'
d['d'] = 34
for k,v in d.items():
  print(k,v)  # This the order is retained