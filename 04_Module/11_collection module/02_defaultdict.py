# defaultdict is a dictionary like object which provides all methods provided by dictionary but takes first argument(default_factory) as default data type for the dictionary. Using defaultdict is faster than doing the same using dict.set_default method.

# This won't through an error in the search key is not found in the dict, it will initialize with the default value set on the lambda function

from collections import defaultdict
# Regular dict first
print('\n# Regular dict first')
d = {'k1':1}
print(d['k1'])
# print(d['k2'])    # KeyError: 'k2' raise an error since the key is not in the dict


# Use defaultdict
print('\n# Use defaultdict')
d = defaultdict(object)
print(d['one'])   # <object object at 0x004245A0>
for item in d:
  print(item)  # one


s = defaultdict(lambda : 0)   # assign this value if there is not keyfound in the dict instead of raising a KeyError as regular dict
print(s['one'])   # 0

s['two'] = 2
print(s)
