from collections import Counter

listed = [1,1,1,1,12,22,2,2,2,4,5,55,4,4,4,3,3,66,6]

# Counter count the number of the element on the list
print('\n# Counter count the number of the element on the list')
print(Counter(listed)) # Counter({1: 4, 4: 4, 2: 3, 3: 2, 12: 1, 22: 1, 5: 1, 55: 1, 66: 1, 6: 1})


# Count the number of repeated letter in the string
print('\n# Count the number of repeated letter in the string')
s = 'qasfhomdmdjshdnslmnbcnmsgdpoewiryfgac'
print(Counter(s))

# Coun number of words in a string
print('\n# # Coun number of words in a string')
s = 'How many times does each word show up in this sentence word word shoe up up'
words = s.split()
print(Counter(words))



# Methods of counter
print('\n# Methods of counter')

# most_common
print('**** most_common => n least common elements')
c = Counter(words)
print(c.most_common(3))  # [('word', 3), ('up', 3), ('How', 1)]

# sum(c.values())
print('\n**** sum(c.values()) => total of all counts')
print(c.values())  # [('word', 3), ('up', 3), ('How', 1)]

# list(c)
print('\n**** list unique elements')
print(list(c))  

# set(c)
print('\n**** set => convert to set')
print(set(c))  

# dict(c)
print('\n**** dict => convert to set')
print(dict(c))  

# c.items
print('\n**** c.items => convert to alist of (elem,cnt) pairs')
print(c.items) 



