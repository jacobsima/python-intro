from collections import namedtuple

# Regular tuple
print('\n# Regular tuple')
t = (1,2,3)
print(t[0])


# namedtuple
print('\n# namedtuple')
Dog = namedtuple('Dog','age breed name')
sam = Dog(age=2,breed='lab',name='Sam')
print(sam)
print(sam.age)