# Whenever a file is printed, it run that .py automatically 
# But from here where it is imported
# this __name__ variable is automatically set firstmodule  for the firstmodule import
# Since it is called at the imported file while Second module __name__ is set to __main__
# since second module is called from the module itself
print('\n# firstmodule, ran here as imported')
import firstmodule   

print('\n# module, ran here witout being imported...')
print('Second module __name__ is: ',__name__) 

firstmodule.main()