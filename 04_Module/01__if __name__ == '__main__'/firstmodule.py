 
print('This is from firstmodule and will always run...')

def main():
# this __name__ variable is automatically set to __main__ is this module is ran without
# being imported.
  print('\n# firstmodule ran here witout being imported...')
  print('firstmodule __name__ is: ',__name__)   # __main__ 

if __name__ == '__main__':
  main()
else:
  print('firstmodule __name__ is: ',__name__)   # firstmodule