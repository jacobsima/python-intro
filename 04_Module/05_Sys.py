# Sys.path
import sys

# Sys.argv
print('\n# Working on sys.argv')
print(sys.argv)   # ['c:/Jacob/python/python-intro/04_Module/04_Sys.py', '12', 'lelo nde','29']
# print(sys.argv[2],type(sys.argv[2]))   # lelo nde <class 'str'>
# print(sys.argv[1],type(sys.argv[1]))   # lelo nde <class 'str'>


# Working on sys.copyright
print('\n# Working on sys.copyright')
print(sys.copyright)



# sys.executable show where and which python version  u ran
print('\n# sys.executable show where and which python version  u ran')
print(sys.version)
print(sys.executable) 
# C:\Users\Jacob\AppData\Local\Programs\Python\Python37-32\python.exe






print('\n')
print('\n# Get System list of all the Paths')
print(sys.path)

# order of search the right path of module to be imported, this is searched in sys.path array
# 0 : is the directory containing the script that we are running
# 1 : directories listed in python path environement variable
# 2 : Standard python library or core library
# 3 : Site-packages for third party library

# Only strings and bytes should be added to sys.path
'''
['c:\\Jacob\\python\\python-intro\\04_Module\\01_Custom_Module_OnSame_Package', 'C:\\Users\\Jacob\\AppData\\Local\\Programs\\Python\\Python37-32\\python37.zip', 'C:\\Users\\Jacob\\AppData\\Local\\Programs\\Python\\Python37-32\\DLLs', 'C:\\Users\\Jacob\\AppData\\Local\\Programs\\Python\\Python37-32\\lib', 'C:\\Users\\Jacob\\AppData\\Local\\Programs\\Python\\Python37-32', 'C:\\Users\\Jacob\\AppData\\Roaming\\Python\\Python37\\site-packages', 'C:\\Users\\Jacob\\AppData\\Roaming\\Python\\Python37\\site-packages\\win32', 'C:\\Users\\Jacob\\AppData\\Roaming\\Python\\Python37\\site-packages\\win32\\lib', 'C:\\Users\\Jacob\\AppData\\Roaming\\Python\\Python37\\site-packages\\Pythonwin', 'C:\\Users\\Jacob\\AppData\\Local\\Programs\\Python\\Python37-32\\lib\\site-packages']


'''

