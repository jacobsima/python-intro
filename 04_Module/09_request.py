import requests

r = requests.get('https://xkcd.com/353/')
print(r) # <Response [200]>
# print(dir(r)) # get all the content method from this object

# Here we only get the HTML of this website/Get the source of the website
# If you wanna parse then use beautifulsoup , which is webscraping
print('\n# Get the source of the website')
print(r.text)



# Download an image from the website
print('\n# Download an image from the website')

# Save the binary data from the website into a new png file
res = requests.get('https://source.unsplash.com/user/erondu/1600x900')
print(res.status_code)
print(res.ok)   # True : anything less than 400 status code will give True
# print(res.headers)

with open('commic.jpeg','wb') as f:
  f.write(res.content)
  print(f'Photo copied on the same directory and filename is: {f.name}')
  print('\n')



# Make Get request to https://httpbin.org/get
print('\n# Make Get request to https://httpbin.org/get')
payload = {
  'page':2,
  'count':25
}
getRes = requests.get('https://httpbin.org/get',params=payload)
print(getRes.url)   # https://httpbin.org/get?page=2&count=25
print(getRes.text) 
"""
{
  "args": {
    "count": "25",
    "page": "2"
  },
  "headers": {
    "Accept": "*/*",
    "Accept-Encoding": "gzip, deflate",
    "Host": "httpbin.org",
    "User-Agent": "python-requests/2.23.0",
    "X-Amzn-Trace-Id": "Root=1-5edea477-2e69a5aceae565800af711b8"
  },
  "origin": "41.246.128.173",
  "url": "https://httpbin.org/get?page=2&count=25"
}  
"""






# Make POST request to https://httpbin.org/post
print('\n# Make Get request to https://httpbin.org/post')
data = {
  'username':"jacob ",
  'email':'jacobsima@ymail.com',
  'password':'123234'
}
postRes = requests.post('https://httpbin.org/post',data=data)
# instead of  postres.text, you can get json response by postres.json()
r_dict = postRes.json()
print(r_dict['form'])  
# instead of using the dump method in res.text , this json() does the job
# {'email': 'jacobsima@ymail.com', 'password': '123234', 'username': 'jacob '}






# Working with httbin Auth route
print('\n# Working with httbin Auth route')
r = requests.get('https://httpbin.org/basic-auth/jacob/testing',auth=('jacob','testing'),timeout=3)
print(r)

