import os
from datetime import datetime
# print(dir(os))   # this will print out all the os method or properties avaible

# get current working directory
print('\n# Get current working directory: ')
print(os.getcwd())

# change the current working directory
print('\n# Change the current working directory:')
os.chdir('/Users/Jacob/Documents/My Received Files/')
print(os.getcwd())

# Lis the file in this changed directory
# a string of path is provided as an argument
# in this case we are working with the current changed working directory
print('\n# Lis the file in this changed directory# ')
print(os.listdir('/Users/Jacob/Documents/My Received Files/'))
print(os.listdir())
'''
['contactlist_Context', 'contactlist_Redux', 'contactmanager', 'contactmanager_Redux', 'pagination', 'pagination-jacob']
'''

# Create a new folder in this directory
os.mkdir('python-script-made-folder-1')
os.mkdir('python-script-made-folder-3')
os.makedirs('python-script-made-folder-2/sub-dir-1')   # able to create sub folder at the same time
os.makedirs('python-script-made-folder-4/sub-dir-2')   # able to create sub folder at the same time


# Remove directory
os.rmdir('python-script-made-folder-3')
os.removedirs('python-script-made-folder-4/sub-dir-2')

# Rename a file
# os.rename('text.txt','react.txt')


# Get file information
print('\n# Get file information')
print(os.stat('react.txt'))
'''
os.stat_result(st_mode=33206, st_ino=42784196460147657, st_dev=1727233758, st_nlink=1, st_uid=0, st_gid=0, st_size=0, st_atime=1591031161, st_mtime=1591031161, st_ctime=1591031161)
'''

print('\n# Get file size')
print(os.stat('react.txt').st_size)


print('\n# Get last update time')

my_time = os.stat('react.txt').st_mtime
print(datetime.fromtimestamp(my_time))     # timestamp = 1591031605.7830002


# os.walk()
# keep a track of all the file in a certain directory
print('\n# os.walk()')
for dirpath,dirnames,filenames in os.walk('/Users/Jacob/Documents/My Received Files/'):
  print('Current Path: ',dirpath)
  print('Directories: ',dirnames)
  print('Files: ',filenames)
  print('\n')


# Access Environment variable
print('\n# Access Environment variable')
print(os.environ['USERNAME'])      # Jacob
print(os.environ['HOME'])      # C:\Users\Jacob
print(os.environ['ANDROID_HOME'])  # C:\Users\Jacob\AppData\Local\Android\Sdk 


print('\n Creating file path')
file_path = os.path.join(os.environ['HOME'],'createdfrom_python_script.txt')
print(file_path)   # 


# Create the file in this path
print('\n# Create the file in this path')
with open(file_path,'w') as f:
  f.write('New text file from python script')
  print('file Created at this path: ',file_path)




# Print current working directory
print('\n# Print current working directory')
print(os.getcwd())
