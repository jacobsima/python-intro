# First-Class Functions:
# A Programming language is said to have first-class functions if it treats functions as first-class citizens

# First-Class Citizen(Programming):
# A first-class citizen (sometimes called first0class objects) in a programming language is an 
# entity which supports all the operations generally available to other entities. These operations typically include bieng passed as an argument, returned from a function and assigned to a variable

def square(x):
  return x*x

f = square  # f become a first class function, as qe can treat this variable as function
print(f(5))


# An high order function is a function that recieve another function as an argument
# or return a function

# Passing another function as argument
print('\n# Passing another function as argument')
# Built map function
print('# Built map function')
def my_map(func,arg_list):         # function is passed without being executed here
  result=[]
  for i in arg_list:
    result.append(func(i))
  return result

squares = my_map(square,[1,2,3,4,5,6])
print(squares)


# Built filter function
print('\n# Built filter function')
def my_filter(func,arg_list):     # function is passed without being executed here
  result = []
  for i in arg_list:
    test = func(i)
    result.append(i) if test else None
  return result

result = my_filter(lambda x: x<=5,[2,1,5,7,8,9])
print(result)


# Returning a function from a function
print('\n# Returning a function from a function')

# logger function
print('\n# logger function')
def logger(msg):

  def log_message():
    print('Log: ',msg)
  
  return log_message

log_hi = logger('Hey')
log_hi()    
# the log_hi() when it is called it still remember the message passed at logger()
# and get treated as the log_message function inside another function
# this is what called a closure


# Practical Example
print('\n# Practical Example')
def htm_tag(tag):
  def wrap_text(msg):
    print('<{0}>{1}</{0}>'.format(tag,msg))
  return wrap_text

print_h1 = htm_tag('h1')    # in here print_h1 is serving as first class function
print_h1('Test Headline!')  # in here as a closure as it remembers the arg of the order func

print_h1 = htm_tag('p')
print_h1('Test Paragraph!')

