# combination:all the different way you can group something,where the order does not matter
# permutation: all the different way to group value, where order does matter

import itertools

my_list = [1,2,3]

# Combination of my_list = [1,2,3], group of 3
print('\n# Combination of my_list = [1,2,3], group of 3')
combinations = itertools.combinations(my_list,3)
for c in combinations:
  print(c)   # (1, 2, 3)

# Combination of my_list = [1,2,3], in 2
print('\n# Combination of my_list = [1,2,3], group of 2')
combinations = itertools.combinations(my_list,2)
for c in combinations:
  print(c)
"""
(1, 2)
(1, 3)
(2, 3)
"""

# Permutation of my_list = [1,2,3], group of 3
print('\n# Permutation of my_list = [1,2,3],group of 3')
permutation = itertools.permutations(my_list,3)
for p in permutation:
  print(p)
"""
(1, 2, 3)
(1, 3, 2)
(2, 1, 3)
(2, 3, 1)
(3, 1, 2)
(3, 2, 1)

"""

# Permutation of my_list = [1,2,3], group of 2
print('\n# Permutation of my_list = [1,2,3], group of 2')
permutation = itertools.permutations(my_list,2)
for p in permutation:
  print(p)
"""
(1, 2)
(1, 3)
(2, 1)
(2, 3)
(3, 1)
(3, 2)
"""



# IN THIS BOTH EXAMPLE COMBINATION WILL BE THE BEST CHOICE
# How many combinations group of my_list = [1,2,3,4,5,6] in group of 3 gives a sum of 10
print('\n# How many combinations group of my_list = [1,2,3,4,5,6] in group of 3 gives a sum of 10 ')
my_list = [1,2,3,4,5,6]
combinations = itertools.combinations(my_list,3)
permutations = itertools.permutations(my_list,3)

print([result for result in combinations if sum(result)==10])  # [(1, 3, 6), (1, 4, 5), (2, 3, 5)]



# How many permutations group of my_list = [1,2,3,4,5,6] in group of 3 gives a sum of 10
print('\n# How many permutations group of my_list = [1,2,3,4,5,6] in group of 3 gives a sum of 10 ')
print([result for result in permutations if sum(result)==10])

''''
[(1, 3, 6), (1, 4, 5), (1, 5, 4), (1, 6, 3), (2, 3, 5), (2, 5, 3), (3, 1, 6), (3, 2, 5), (3, 5, 2), (3, 6, 1), (4,1, 5), (4, 5, 1), (5, 1, 4), (5, 2, 3), (5, 3, 2), (5, 4, 1), (6, 1, 3), (6, 3, 1]
'''


# Word game:  Look for any arragment of "plmeas" is equal or match "sample"'
print('\n# Word game:  Look for any arragment of "plmeas" is equal or match "sample"')
word = 'sample'
my_letters = 'plmeas'

combinations = itertools.combinations(my_letters,6)
permutations = itertools.permutations(my_letters,6)

# Be more careful working with permutation due to computer power as you gonna run a lot of code 
# under the wood
for i,p in enumerate(permutations):
  if ''.join(p) == word:
    print(f'at index {i} the value Match!')
    break

  else:
    print('No Match!')

