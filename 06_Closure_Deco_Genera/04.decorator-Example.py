# Common use of decorator in python is logging and timing operation of function ran
import time
from functools import wraps
import logging

# Logging decorator
def my_logger(orig_func):

  logging.basicConfig(filename=f'{orig_func.__name__}',level=logging.INFO)

  @wraps(orig_func)
  def wrapper(*args,**kwargs):
    logging.info(f'Ran with args: {args}, and kwargs: {kwargs}')
    return orig_func(*args, **kwargs)
  
  return wrapper


# Time decorator
def my_timer(orig_func):

  @wraps(orig_func)
  def wrapper(*args, **kwargs):
    t1 = time.time()
    result =  orig_func(*args, **kwargs)
    t2 = time.time()-t1
    print(f'{orig_func.__name__} ran in: {t2}')
    return result
  return wrapper





# Logging decorator
print('\n# Logging decorator')

@my_timer
@my_logger
def display_info(name,age):
  time.sleep(2)
  print(f'display_info ran with arguments({name},{age})')

display_info('Jacob',30)












