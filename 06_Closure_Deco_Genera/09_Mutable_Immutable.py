# An immutable object is an object whose state cannot be modified after it is created
# this is in constract to mutable object, which can be modified after it is created.

# String is immutable
print('\n# String is immutable')
a = 'corey'
print(a)
print(f'Address of a is: {id(a)}')

a = 'jacob'
print(a)
print(f'Address of a is: {id(a)}')

# a[0] = 'C'   # TypeError: 'str' object does not support item assignment

# list is mutable
print('\n# list is mutable')
b = [1,2,4,7,6,5,2]
print(b)
b[4] = 43
print(b)


# Concat a lot of string become an issue in the memory due to the id memory created
print('\n# Concat a lot of string become an issue in the memory due to the id memory created ')
employees = ['Corey','Jacob','Rick','Steve','Carl','Adam']
output = '<ul>\n'
for employee in employees:
  output += f'\t<li>{employee}</li>\n'
  print(f'Address of outputis {id(output)}')

output += '</ul>'

print(output)
print('\n')
