# https://www.python-course.eu/python3_decorators.php
# https://www.codementor.io/@sheena/advanced-use-python-decorators-class-function-du107nxsv

# A decorator is just a function that takes another function as an argument, adds some kind of functionality, and then returns another function
# Check flask and Django to see how decorator works

# 1. Function Decorator
print('\n# 1. Function Decorator')
def decoratror_function(original_function):
  def wrapper_function(*args, **kwargs):
    print(f'wrapper executed this before {original_function.__name__}')
    return original_function(*args, **kwargs)
  return wrapper_function


# Decorator example One
print('\n# Decorator example One')
def display1():
  print('Display1 function ran')

decorated_display = decoratror_function(display1)
decorated_display()



# Decorator example Two
print('\n# Decorator example Two')
def display2():
  print('Display2 function ran')
display2 = decoratror_function(display2)
display2()



# Decorator example Three
print('\n# Decorator example Three')

@decoratror_function
def display3():
  print('Display3 function ran')

display3()



# Add *args and **kwargs : to allow the first class function to pass *args or **kwargs
print('\n# Add *args and **kwargs : to allow the first class function to pass *args or **kwargs')

@decoratror_function
def display_info(name,age):
  print(f'display_info ran with arguments ({name},{age})')

display_info('jacob',30)




# 2. Class Decorator
print('\n# 1. Class Decorator')


class decorator_class(object):
  # this gonna tie the function with an instance of this class
  def __init__(self,original_function):
    self.original_function = original_function

  
  # mimic the functionaly of the wrapper and the original function
  def __call__(self,*args, **kwargs):
    print(f'the call method executed this before {self.original_function.__name__}')
    return self.original_function(*args, **kwargs)



# Decorator example Four
print('\n# Decorator example Four')

@decorator_class
def display4():
  print('Display4 function ran')

display4()


