// First-Class Functions:
// A Programming language is said to have first-class functions if it treats functions as first-class citizens

// First-Class Citizen(Programming):
// A first-class citizen (sometimes called first0class objects) in a programming language is an 
// entity which supports all the operations generally available to other entities. These operations typically include bieng passed as an argument, returned from a function and assigned to a variable

function square(x){
  return x*x;
}

let f = square
console.log(f(5))  // f become a first class function, as it can treat this variable as function


// An high order function is a function that recieve another function as an argument
// or return a function

// Passing another function as argument
console.log('// Passing another function as argument')
// Built a map function
console.log('// Built a map function')
function my_map(func,arg_list){         // function is passed without being executed here
  let result = []
  for(let i=0; i< arg_list.length;i++){
    result.push(func(arg_list[i]))
  }
  return result
}

let result = my_map(square,[1,2,3,4,5,6])
console.log(result)

console.log('// Built a filter function')
function my_filter(func,arr_list){      // function is passed without being executed here
  let result = []
  for(let i=0; i< arr_list.length;i++){
    let test = func(arr_list[i])
    test ? result.push(arr_list[i]):''
  }
  return result
}

result = my_filter(x => x <= 5,[1,2,3,4,5,6,7,8,9])
console.log(result)



// Returning a function from a function
console.log('// Returning a function from a function')
console.log('// Logger message')
function logger(msg){
  function log_message(){
    console.log(`Log: ${msg}`)
  }

  return log_message
}

let log_hi = logger('hey')
log_hi()


// Practical Example
console.log('// Practical Example')
function html_tag(tag){
  function wrap_text(msg){
    console.log(`<${tag}> ${msg} </${tag}>`)
  }
  return wrap_text
}

print_h1 = html_tag('h1')
print_h1('Test Headline!')
print_h1('Another Test Headline!')


print_h1 = html_tag('p')
print_h1('Test Paragraph!')


