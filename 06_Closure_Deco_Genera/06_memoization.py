# Memoization is an optimization technique used primary to speed up computer
# programs by storing the results of expensive function calls and returning the cached
# when the same inputs occur again

import time

ef_cache = {}

def expensive_func(num):
  if num in ef_cache:
    return ef_cache[num]

  print(f'Computing {num}...')
  time.sleep(1)
  result = num*num
  ef_cache[num] = result   # add result to the cache
  return result

result = expensive_func(4)
print(result)

result = expensive_func(10)   
print(result)


# instead of running this again(this function with the same value again) or make the program sleep again
# we can use memoization to save this to a cache so that next time
# or next call we do not wait for this waiting time
# we can just return the value from the cache
result = expensive_func(4)
print(result)

result = expensive_func(10)
print(result)