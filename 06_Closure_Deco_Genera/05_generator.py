nums = [1,2,3,4,5]
# Normal forloop within a func
print('\n# Normal forloop within a func')
def square_numbers(nums):
  result = []

  for i in nums:
    result.append(i*i)
  return result

my_nums = square_numbers(nums)
# my_nums = [ x*x for x in nums]
print(my_nums)


# Convert normal forloop within a func to generator
print('\n# Convert normal forloop within a func to generator')

def square_nums(nums):
  for i in nums:
    yield (i*i)      # This yield make this function a generator
my_nums2 = square_nums(nums)

print(my_nums2) # <generator object square_nums at 0x01D38FB0>

print(next(my_nums2))      # 1
print(next(my_nums2))      # 4




# *********** Generator Example **********
print('\n# *********** Generator Example **********')
import memory_profiler as mem_profile
import random
import time

names = ['John','Corey','Adam','Steve','Rick','Thomas']

majors = ['Math','Engineering','CompSci','Arts','Business']

memor = mem_profile.memory_usage()
print(f'Memory (Before): {memor}Mb')

def people_list(num_people):
  result = []
  for i in range(num_people):
    person ={
      'id':i,
      'name':random.choice(names),
      'major':random.choice(majors)
    }
    result.append(person)

  return result




def people_generator(num_people):
  for i in range(num_people):
    person ={
      'id':i,
      'name':random.choice(names),
      'major':random.choice(majors)
    }
    yield person


# time.perf_counter
# time.process_time

# t1 = time.perf_counter()  
# people = people_list(1000000)
# t2 = time.perf_counter()


t1 = time.perf_counter()  
people = people_generator(1000000)
t2 = time.perf_counter()

t = t2-t1
memor = mem_profile.memory_usage()
print(f'Memory (After): {memor}Mb')
print(f'Took {t} seconds')

