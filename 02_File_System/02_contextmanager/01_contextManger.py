from contextlib import contextmanager
import os

# Create Context manager using a class
print('\n# Create Context manager using a class')
class Open_File():

  def __init__(self,filename,mode):
    self.filename = filename
    self.mode = mode

  
  def __enter__(self):
    self.file = open(self.filename,self.mode)
    return self.file


  # teardown  
  def __exit__(self,exc_type,exc_val,traceback):
    self.file.close()

with Open_File('sample.txt','w') as f:
  f.write('Testing...')

print(f.closed)



# Create contexmanager using function
print('\n# Create contexmanager using function')
@contextmanager
def open_file(file,mode):
  try:
    f = open(file,mode)
    yield f    # return f
  finally:
    f.close()


with open_file('sample2.txt','w') as f:
  f.write('This is contextmanage done with function')


print(f.closed)


# Practical example of context managment
print('\n# Practical example of context managment')

"""
cw = os.getcwd()
os.chdir('Sample-dir-one')
print(os.listdir())
os.chdir(cw)
"""

@contextmanager
def change_dir(destination):
  try:
    cw = os.getcwd()
    os.chdir(destination)
    yield
  finally:
    os.chdir(cw)

with change_dir('Sample-dir-one') :
  print(os.listdir())

with change_dir('Sample-dir-two') :
  print(os.listdir())





