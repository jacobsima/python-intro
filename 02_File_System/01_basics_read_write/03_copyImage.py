# Copy a binary file 
# Create a new image file from one Image file
print('\n# Create a new image file from one Image file, using foorloop')
with open('bootcamp.jpg','rb') as rf:     # read binary mode
  with open('bootcamp-2.jpg','wb') as wf:
    for line in rf:
      wf.write(line)


print('\n# Create a new image file from one Image file using chunk size')
with open('bootcamp.jpg','rb') as rf:     # read binary mode
  with open('bootcamp-3.jpg','wb') as wf:
    chunk_size = 4096
    rf_chunk = rf.read(chunk_size)
    while len(rf_chunk) > 0:
      wf.write(rf_chunk)
      rf_chunk = rf.read(chunk_size)   # read the next chunk to avoid infinite loop