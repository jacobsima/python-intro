    Method	                      Description
  =========                   ==================

    file.close()	            Closes the file.

    file.flush()	            Flushes the internal buffer.

    next(file)	               Returns the next line from the file each time it is called.

    file.read([size])	         Reads at a specified number of bytes from the file.

    file.readline()	           Reads one entire line from the file.

    file.readlines()	         Reads until EOF and returns a list containing the lines.

    file.seek(offset, from)	   Sets the file's current position.

    file.tell()	               Returns the file's current position

    file.write(str)	            Writes a string to the file. There is no return value.