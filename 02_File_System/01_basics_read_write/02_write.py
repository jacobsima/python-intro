# Write in a file
# w => if file does not exist create a new one else overwrite the existing file
print('\n# Write in a file')
with open('text2.txt','w') as f:
  f.write('Test')
  # f.seek(0)     # not mostly used in file write, causes a lot of confusion
  # f.write('R')

with open('text.txt','r') as rf:
  with open('textcopy.txt','w') as wf:
    for line in rf:
      wf.write(line)

