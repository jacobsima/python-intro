# File objects
# ! Not the recommendate way to open a file but let learn it
print('\n# ! Not the recommendate way to open a file but let learn it')
fk = open('text.txt','r')    # default mode is reading
print(fk.name)         # text.txt
print(fk.mode)         # r
fk.close()


# Best way to open a file is to use context manager
print('\n# Context manager file open')
print('\n# Best way to open a file is to use context manager')
with open('text.txt','r') as f:
  a_contents = f.read()
  print('\n***** f.read()')
  print(a_contents)            # print the whole file content
  '''
    1) this is a test file!
    2) with multiple lines of data
    3) third lines
    4) fourth lines
    5) fith lines
    6) sixth lines
    7) seveth lines
    8) eighth lines
    9) nith lines
    10 tenth lines
  '''

print(f.closed)    # True
# file already closed but you still have access to the f variable
# you can not read from here hence once outside this block the file is closed
# automatically
print('\n')

# f.readlines()
print('***** f.readlines()')
with open('text.txt','r') as f:
  b_contents = f.readlines()
  print(b_contents)

  """
  ['1) this is a test file!\n', '2) with multiple lines of data\n', '3) third lines\n', '4) fourth lines\n', '5) fith lines\n', '6) sixth lines\n', '7) seveth lines\n', '8) eighth lines\n', '9) nith lines\n', '10 tenth lines\n']

  """
  print('\n')

# f.readline()
  print('***** f.readline()')
with open('text.txt','r') as f:
  c_contents = f.readline()   # this read the first line 
  print(c_contents,end='')    # add end = '' to avoid printing space between lines
  c_contents = f.readline()   # this read the second line 
  print(c_contents,end='') 
  '''
    1) this is a test file!
    2) with multiple lines of data
  '''

# Iterate within in the file
  print('***** Iterate within in the file')
with open('text.txt','r') as f:
  for line in f:
    print(line,end='')
    '''
    1) this is a test file!
    2) with multiple lines of data
    3) third lines
    4) fourth lines
    5) fith lines
    6) sixth lines
    7) seveth lines
    8) eighth lines
    9) nith lines
    10 tenth lines
    '''
# Read a certain size of character in the file
print('\n# Read a certain size of character in the file')
with open('text.txt','r') as f:
  f_contents = f.read(100)
  print(f_contents)
  '''
  1) this is a test file!
  2) with multiple lines of data
  3) third lines
  4) fourth lines
  5) fith lines
  '''
  f_contents = f.read(100)
  print(f_contents)
  '''
  6) sixth lines
  7) seveth lines
  8) eighth lines
  9) nith lines
  10 tenth lines
  '''

# Read a certain size of character in the file while Iterating
print('\n# Read a certain size of character in the file while Iterating')
with open('text.txt','r') as f:
  size_to_read = 10
  f_contents = f.read(size_to_read )

  print(f.tell())  # tell the current position of the cursor

  while len(f_contents) > 0: # Since at end of the content, the line length is zero

    print(f_contents,end='*')

    # to avoid the infinite loop, to call a chunk of 10 each time until file last line length is zero

    f_contents = f.read(size_to_read ) 





# Using tell and seek
print('\n# # Using tell and seek')
with open('text.txt','r') as f:
  size_to_read = 10
  f_contents = f.read(size_to_read )
  print(f_contents,end='')

  print('\n***** f.tell(): ')
  print(f.tell())  # tell the current position of the cursor  # 10

  print('***** f.seek(0): ')
  f.seek(0)    # reset the start reading point
  f_contents = f.read(size_to_read )
  print(f_contents,end='')

  print('\n***** f.tell(): ')
  print(f.tell())  # 10

 
