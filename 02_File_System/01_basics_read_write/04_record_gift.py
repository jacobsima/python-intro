import os

names = []
presents = []

while True:
  print("Menu")
  print("**************************************")
  print("This program is to record a christmas presents shopping list for family/friends")
  print("1. Enter Names and Presents and Save")
  print("2. View Names and Presents from Sabed File")
  option = int(input())

  if option == 1:
    print("\n")
    number = int(input("How many people are you buying for\n"))

    for x in range(number):
      name = input("Enter Name\n")
      names.append(name)
      present = input("Enter present\n")
      presents.append(present)
    
    with open('birthday.txt','w') as f:
      for name,present in zip(names,presents):
        f.write(name)
        f.write(', ')
        f.write(present)
        f.write('\n')
      
     
    print("\n") 
  if option == 2:
    print("\n")
    
    if os.path.exists('birthday.txt'):
      with open('birthday.txt','r') as f:
        for line in f:
          name,present = line.split(',')
          print(name,',',present, end='\n')
    else:
      print("No data found, Please Add name and present")

    
    print("\n")
