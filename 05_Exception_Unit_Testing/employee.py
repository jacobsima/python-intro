import requests

class Employee:
  # A sample Employee class

  raise_amt = 1.05

  def __init__(self,first,last,pay):
    self.first = first
    self.last = last
    self.pay = pay


  @property
  def email(self):
      return f'{self.first}.{self.last}@gmail.com'

  @property
  def fullname(self):
      return f'{self.first} {self.last}'

  def apply_raise(self):
    self.pay = int(self.pay * Employee.raise_amt)

  
  # Mocking function
  # Since this response depends on another website we do not want the test
  # to fail in case this website is down, therefore we gonna test if 
  # this get function was called.
  def monthly_schedule(self,month):
    response = requests.get(f'http://company.com/{self.last}/{month}')
    if response.ok:
      return response.text 
    else:
      return 'Bad Response'