import unittest
import calc



class TestCalc(unittest.TestCase):
  
  def test_add(self): 
    self.assertEqual(calc.add(10,5),15)
    self.assertEqual(calc.add(-1,1),0)
    self.assertEqual(calc.add(-1,-1),-2)


  def test_subtraction(self): 
    self.assertEqual(calc.subtraction(10,5),5)
    self.assertEqual(calc.subtraction(-1,1),-2)
    self.assertEqual(calc.subtraction(-1,-1),0)



  def test_multiple(self): 
    self.assertEqual(calc.multiple(10,5),50)
    self.assertEqual(calc.multiple(-1,1),-1)
    self.assertEqual(calc.multiple(-1,-1),1)



  def test_divide(self): 
    self.assertEqual(calc.divide(10,5),2)
    self.assertEqual(calc.divide(-1,1),-1)
    self.assertEqual(calc.divide(-1,-1),1)
    self.assertEqual(calc.divide(5,2),2.5)

    # self.assertRaises(ValueError,calc.divide,10,0)

    # Prefered to use context manager to test exception
    with self.assertRaises(ValueError):
      calc.divide(10,0)











# Run: python -m unittest calc_test.py : to run this unnittest file


# But if you wanna run script without inserting the -m unittest in the terminal then 
# check the set up below

if __name__ == '__main__':
  unittest.main()
