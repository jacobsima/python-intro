import traceback
import sys
# 1. Exception raise or caught by Python program
print('\n# 1. Exception raise or caught by Python program')
try:
  f = open('text.txt')
  # var = ba
except FileNotFoundError as e:
  print(e)

except Exception as e:
  print(e)
else:
  # if not exception then execute this code
  # if there was not error, meaning the try went well then run this else
  print(f.read())
  f.close()

finally:
  # This run no matter what
  # Do something here regardless of the code throw an exception of not
  print('Finally always runs not matter what')




#2. Exception raise by the programmer
print('\n# 2. Exception raise by the programmer')
try:
  f = open('lesson.txt')
  if f.name == 'lesson.txt':
    # raise NotImplementedError('subclass must implement this abstract method')
    raise Exception('File error...')
except Exception as e:
  print(e)



#3 . Raise Custom Exception
print('\n#3 . Raise Custom Exception')
class ResponseError(Exception):
  def __init__(self,message,response_code,tracebacker):
    self.msg = message
    self.response_code = response_code
    self.tracebacker = tracebacker.extract_stack()

  def __str__(self):
    return f'''
This error is generated from a custom error builder
message: {self.msg}
errorCode: {self.response_code}
TraceBack Error list: {self.tracebacker}
'''
# Example One
try:
  f = open('lesson.txt')
  if f.name == 'lesson.txt':
    raise ResponseError('Filename is equal to the lesson.txt',401,traceback)
except Exception as e:
  print(e)


# Example Two
try:
  f = open('lesson.txt')
  if f.name == 'lesson.txt':
    raise ResponseError('This file exists and we mimic an error',403,traceback)
except Exception as e:
  print(e)



# Example Three
try:
    t = 4/0
except Exception as e:
    import traceback
    s = traceback.format_exc()
    k = traceback.extract_stack()
    print('From Example three custom Exception: ',k)


      
      









