import unittest
from unittest.mock import patch
# patch can be used as a decorator or a context manager
from employee import Employee

# The test does not run in order
class TestEmployee(unittest.TestCase):
  
  @classmethod
  def setUpClass(cls):
    # Todo something once at the beginning of the test
    # maybe populate database to run test 
    # or create a file to run test
    print('\nSetup class...')


  @classmethod
  def tearDownClass(cls):
    # Todo something once at the end of the test
    # maybe clear database used to run the test on
    # or delete a file used to run the test on
    print('\n\ntearDownClass...')




  # name of this function character sensitive pyhton uses this function under the wood
  # This will run the code before every single test
  def setUp(self):
    print('\nsetUp')
    self.emp_1 = Employee('Corey','Schafer',5000)
    self.emp_2 = Employee('Sue','Smith',6000)

  # name of this function character sensitive pyhton uses this function under the wood
  # This will run the code after every single test
  def tearDown(self):
    print('tearDown')
    pass

  def test_email(self):
    print('test_email')
    self.assertEqual(self.emp_1.email, 'Corey.Schafer@gmail.com')
    self.assertEqual(self.emp_2.email, 'Sue.Smith@gmail.com')

    self.emp_1.first = 'John'
    self.emp_2.first = "Jane"

    self.assertEqual(self.emp_1.email,'John.Schafer@gmail.com')
    self.assertEqual(self.emp_2.email,'Jane.Smith@gmail.com')
 
  def test_fullname(self):
    print('test_fullname')
    self.assertEqual(self.emp_1.fullname,'Corey Schafer')
    self.assertEqual(self.emp_2.fullname,'Sue Smith')

    self.emp_1.first = 'John'
    self.emp_2.first = "Jane"

    self.assertEqual(self.emp_1.fullname,'John Schafer')
    self.assertEqual(self.emp_2.fullname,'Jane Smith')

  def test_apply_raise(self):
    print('test_apply_raise')
    self.emp_1.apply_raise()
    self.emp_2.apply_raise()

    self.assertEqual(self.emp_1.pay,5250)
    self.assertEqual(self.emp_2.pay,6300)

  def test_monthly_schedule(self):
    # patch used as a content manager
    # use requests.get in the employee module
    with patch('employee.requests.get') as mocked_get:
      mocked_get.return_value.ok = True
      mocked_get.return_value.text = 'Success'

      schedule = self.emp_1.monthly_schedule('May')
      mocked_get.assert_called_with('http://company.com/Schafer/May')


      self.assertEqual(schedule,'Success')







  










if __name__ == '__main__':
  unittest.main()













# # Test Work but we gonna applu DRK
# class TestEmployee(unittest.TestCase):

#   def test_email(self):
#     emp_1 = Employee('Corey','Schafer',5000)
#     emp_2 = Employee('Sue','Smith',6000)

#     self.assertEqual(emp_1.email, 'Corey.Schafer@gmail.com')
#     self.assertEqual(emp_2.email, 'Sue.Smith@gmail.com')

#     emp_1.first = 'John'
#     emp_2.first = "Jane"

#     self.assertEqual(emp_1.email,'John.Schafer@gmail.com')
#     self.assertEqual(emp_2.email,'Jane.Smith@gmail.com')

  
#   def test_fullname(self):
#     emp_1 = Employee('Corey','Schafer',5000)
#     emp_2 = Employee('Sue','Smith',6000)

#     self.assertEqual(emp_1.fullname,'Corey Schafer')
#     self.assertEqual(emp_2.fullname,'Sue Smith')

#     emp_1.first = 'John'
#     emp_2.first = "Jane"

#     self.assertEqual(emp_1.fullname,'John Schafer')
#     self.assertEqual(emp_2.fullname,'Jane Smith')


#   def test_apply_raise(self):
#     emp_1 = Employee('Corey','Schafer',5000)
#     emp_2 = Employee('Sue','Smith',6000)

#     self.assertEqual(emp_1.fullname,'Corey Schafer')
#     self.assertEqual(emp_2.fullname,'Sue Smith')

#     emp_1.apply_raise()
#     emp_2.apply_raise()

#     self.assertEqual(emp_1.pay,5250)
#     self.assertEqual(emp_2.pay,6300)

