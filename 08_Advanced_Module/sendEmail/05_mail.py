import smtplib
import os
from email.message import EmailMessage
import imghdr

email_address = os.environ['EMAIL_ADDRESS']
email_pass = os.environ['EMAIL_PASS']

contacts = ['simajacob2011@gmail.com','simatest2011@gmail.com','jacobssima@ymail.com']

msg = EmailMessage()
msg['Subject'] = 'Check pics'
msg['From'] = email_address
msg['To'] = ','.join(contacts)    # to send to multipe contacts
msg.set_content('This is a plain text email')
msg.add_alternative("""\
<!DOCTYPE html>
<html>
 <body>
    <h1 style="color:SlateGray;"> This is an HTML Email </h1>
    <p> 
      <a href="www.google.com"> Visit google </a>
    </p>
   
  </body>
</html>
  """,subtype='html')

with smtplib.SMTP_SSL('smtp.gmail.com',465) as smtp:
  smtp.login(email_address,email_pass)
  smtp.send_message(msg)


