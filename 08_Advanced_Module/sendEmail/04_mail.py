import smtplib
import os
from email.message import EmailMessage
import imghdr

email_address = os.environ['EMAIL_ADDRESS']
email_pass = os.environ['EMAIL_PASS']

msg = EmailMessage()
msg['Subject'] = 'Check pics'
msg['From'] = email_address
msg['To'] = 'simajacob2011@gmail.com'
msg.set_content('Image attached...')

files = ['nkufi.pdf']

for file in files:
  with open(file,'rb') as f:
    file_data = f.read()
    file_name = f.name
  
  # application and octet-stream are for generic file
  msg.add_attachment(file_data,maintype='application',subtype='octet-stream',filename=file_name)

with smtplib.SMTP_SSL('smtp.gmail.com',465) as smtp:
  smtp.login(email_address,email_pass)
  smtp.send_message(msg)


