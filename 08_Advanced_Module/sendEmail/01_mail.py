import smtplib
import os

email_address = os.environ['EMAIL_ADDRESS']
email_pass = os.environ['EMAIL_PASS']

with smtplib.SMTP('smtp.gmail.com',587) as smtp:
  smtp.ehlo()     # identify this app with the mail server used in the script
  smtp.starttls() # to encrypt the traffic
  smtp.ehlo() # re-run this command to re-identify as enctypted connection

  smtp.login(email_address,email_pass)

  subject = 'Grab dinner this weekend'
  body = 'How about dinner at 6pm this Saturday'

  msg = f'Subject:{subject}\n\n{body}'

  smtp.sendmail(email_address,'simajacob2011@gmail.com',msg)

