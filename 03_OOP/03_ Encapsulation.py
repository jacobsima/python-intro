# Encapsulation
print('\n# Typical Class')
class BankAccount():

  def __init__(self,account_name='Current Account',balance=200):
    self.account_name = account_name
    self.balance = balance

myAccount = BankAccount()
print('*** BankAccount class')
print(myAccount.account_name)   # Current Account
print(myAccount.balance)        # 200


print('\n')

print('\n# Encapsulation example')
class BankAccountEnc():

  def __init__(self,account_name='Current Account',balance=200):
    self.__account_name = account_name #__ make this property private only access in class
    self.__balance = balance  #__ make this property private only access in class
  
  # getter and setter Methods to access private properties within the class
  def balanceGetter(self):
    print(self.__account_name)
  
  def balanceSetter_withdraw(self,value):
    if value < self.__balance:
      self.__balance = self.__balance - value
      print('New balance: ',self.__balance)
    else:
      print('You do not have enough funds!')


myAccount2 = BankAccountEnc()
print('*** BankAccountEnc class')
# print(myAccount2.account_name) 
# # AttributeError: 'BankAccountEnc' object has no attribute 'account_name'
# print(myAccount2.balance)  
# #AttributeError: 'BankAccountEnc' object has no attribute 'balance'   


while True:
  print("1. Check Account Balance")
  print("2. Withdraw Funds")
  menu_option = int(input())
  if menu_option == 1:
    myAccount2.balanceGetter()
  elif menu_option == 2:
    value = int(input("How much would you like to withdraw"))
    myAccount2.balanceSetter_withdraw(value)
  else:
    print('Wrong menu choice!')
