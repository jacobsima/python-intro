# Example built in class or object
print('\n# Example built in class or object')
mylist = [1,2,3,4]
myset = set()
print(type(mylist))    # <class 'list'>
print(type(myset))     # <class 'set'>

# User Define Class
print('\n# User Define Class')
class Sample():
  pass

mySample = Sample()
print(type(mySample))   # <class '__main__.Sample'>


# Dog class
print('\n# Dog class')
class Dog():
  # Class Object Attribute
  # Same for any instance of a class
  species = 'mammal'

  # Attributes
  # We take in the argument
  # Assign it using self.attribute_name
  def __init__(self,breed,name,spots):
    self.breed = breed
    self.name = name
    # Expect boolean True/False for spots
    self.spots = spots

  # Methods ---> Operations/Actions
  def bark(self,number):
    print(f'Woof! my name is {self.name} and the number is {number}')
  
myDog = Dog(breed='lab',name='sammy',spots=False)

# Properties
print('\n# Properties')
print(type(myDog))
print(myDog.breed)    # lab
print(myDog.name)     # sammy
print(myDog.species)  # mammal 


# Methods
myDog.bark(2)   # Woof! my name is sammy and the number is 2

# Circle class
print('\n# Circle class')
class Circle():
  # Class object attribute
  pi = 3.14

  def __init__(self,radius=1):
    self.radius = radius
    self.area = (radius**2)*self.pi
    # or
    self.area2 = (radius**2)*Circle.pi
  
  # Methods
  def getCirconference(self):
    return self.radius*self.pi*2

mycircle = Circle()
print('mycircle instance')
print(mycircle.getCirconference())
print(mycircle.area)
print(mycircle.pi)
print(mycircle.radius)

print('mycircle2 instance')
mycircle2 = Circle(3)
print(mycircle2.getCirconference())
print(mycircle2.area)
