class Employee:
  num_of_emps = 0
  raise_amt = 1.04  # 4%

  def __init__(self,first,last,pay):
    self.first = first
    self.last = last
    self.email = f'{first}.{last}@gmail.com'
    self.pay = pay

    Employee.num_of_emps += 1
    # self.num_of_emps += 1
  
  # regular methods of class takes an instance as first argument,which is: self
  def fullname(self):         # regular method   
    return f'{self.first} {self.last}'
  
  def appy_raise(self):       # regular method
    self.pay = int(self.pay * self.raise_amt)


  # class method takes class as the first argument 
  # in this classmethod function you are working with the class
  # instead of the instance
  @classmethod
  def set_raise_amt(cls,amount):   # do not use the word class as argument, use cls instead
    cls.raise_amt = amount


  # Create Alternative Employee Constructor that recieve a string
  @classmethod
  def from_str(cls,emp_str):
    first,last,pay = emp_str.split('-')
    return cls(first,last,pay)

  # Static function does not take self or class as argument
  # You provide it with its own argument
  # Use this static method if you do not access any self. or Employee properties
  @staticmethod
  def is_workday(day):
    if day.weekday() == 5  or day.weekday() == 6:
      return False
    return True







employee_1 = Employee('Jacob','Sima',50000)
employee_2 = Employee('Test','Employee',10000)

print('\n# Before using classmethod')
print(Employee.raise_amt)
print(employee_1.raise_amt)
print(employee_2.raise_amt)


print('\n# Afetr using classmethod')
# it gets passed the class as first argument automatically
# by using the class name eg:Employee
# here we are working with the class instead of the instance
Employee.set_raise_amt(1.05)     # 5%

print(Employee.raise_amt)
print(employee_1.raise_amt)
print(employee_2.raise_amt)


# String data receive to for instatiate a new employee   from Employee Class
print('\n# String data receive to for instatiate a new employee object  from Employee Class')
empstr_3 = 'John-Doe-70000'
first,last,pay = empstr_3.split('-')
employee_3 = Employee(first,last,pay)
print(employee_3.raise_amt)
print(employee_3.first)
print(employee_3.email)

# Use alternative Constructor that allow them to send a string and we can create a new employee for them instead 
print('\n# Use classmethod to create an alternative Constructor that allow them')
print('# to send a string and we can create a new employee for them instead ')

empstr_4 = 'Steve-Benard-20000'
employee_4 = Employee.from_str('Steve-Benard-20000')
print(employee_4.first) 
print(employee_4.email) 
print(employee_4.pay) 


# Static Method in a class
# It is just a simple function connect to the class itself only
print('\n# Static Method in a class')
import datetime
my_date = datetime.date(2020,2,16)
print(Employee.is_workday(my_date))

