# Class creation
print('\n# Class creation')
class Animal():

  def __init__(self):
    print('Animal Created')
  
  def who(self):
    print('I am an animal')
  def eat(self):
    print('I am eating')

print('\n* myAnimal instance')
myAnimal = Animal()
myAnimal.who()
myAnimal.eat()



# Inheritance
print('\n# Inheritance')
class Dog(Animal):
  def __init__(self):
    Animal.__init__(self)
    print('Dog instance created and inherited from Animal class')
  
  # Overwrite the parent method
  def who(self):
    print('I am a dog')

  # create or add a new methods in Dog
  def bark(self):
    print('Woof')

print('\nDog class inherited from Animal')
myDog = Dog()
myDog.eat()
myDog.who()
myDog.bark()


# isinstance and issubclass
print('\n# isinstance and issubclass')
print(isinstance(myAnimal,Animal))   # True
print(isinstance(myAnimal,Dog))      # False
print(isinstance(myDog,Dog))         # True
print(isinstance(myDog,Animal))      # True
print(issubclass(Dog,Animal))       # True
print(issubclass(Animal,Dog))       # False