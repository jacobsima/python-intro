
# print('\n # POLYMORPHISM ')
class Animal:
  def __init__(self,name=''):
    self.name = name
  def talk(self):
    pass


class Doggy(Animal):
  
  def __init__(self,name):
    self.name = name
    Animal.__init__(self)

  def speak(self):
    return self.name + ' says woof!'


class Catty():

  def __init__(self,name):
    self.name = name
    Animal.__init__(self)
  
  def speak(self):
    return self.name + ' says meow!'

dog = Doggy('dog')
cat = Catty('cat')

# From Doggy class
print('\n# From Doggy class')
print(dog.speak())

# From Catty class
print('\n# From Catty class')
print(cat.speak())




# # For loop in POLYMORPHISM
print('\n# For loop in POLYMORPHISM')
for pet in[cat,dog]:
  print(type(pet))
  print(pet.speak())





# Abstract class where it is more used
print('\n# This is how POLYMORPHISM  is mostly implemtented')
print('# Abstract class where it is more used')
class Animals():
  def __init__(self,name):
    self.name =name
  
  def speak(self):
    raise NotImplementedError('subclass must implement this abstract method')

# if you create this instance something that we are not expecting to do
# It is expecting you to inherit the Animal class and overwrite the speak method

# myAnimal = Animals('lana')   # This is then deleted
# print(myAnimal.speak())

# Then we get an error: raise NotImplementedError('subclass must implement this abstract method')


# Now how to you use it
# Note: This speak can be an open file operation, and you would like to open csv,pdf or excel file
# you may create different class that for different file and inherit th open file class from the main one
# This is how POLYMORPHISM  is mostly implemtented
class Dogger(Animals):
  def speak(self):
    return self.name+' says woof!'

class Catter(Animals):
  def speak(self):
    return self.name+' says meow!'

isis =  Dogger('isis')
fido =  Catter('fido')

print(isis.speak())
print(fido.speak())