# POLYMORPHISM mean many form  
# The same operation results in different behaviours depending on the type of data it is given  
# The same idea(same operation-different behaviour) can be applied to our own classes and objects
# You gonna use this later in your Pyhton Career
# eg: 1 + 2 = 3; 'a' + 'b' = 'ab'

# BankAccount
class BankAccount():
  # This is a bank account class
  def __init__(self, account_name='Bank Account',balance=1500):
    self.__account_name = account_name
    self.__balance = balance
  
  def deposit(self,value):
    self.__balance = self.__balance + value
    print('\nFrom BankAccount')
    print('You now have: ',self.__balance)
  
  def withdraw(self,value):
    self.__balance = self.__balance - value
    print('\nFrom BankAccount')
    print("You now have: ",self.__balance)
  
# CurrentAccount
class CurrentAccount(BankAccount):
  def __init__(self,account_name = "Current Account",balance=1500):
    self.__account_name = account_name
    self.__balance = balance
    BankAccount.__init__(self)

  def withdraw(self,value):
    print('\nCurrentAccount')
    if value > 1000:
      print("You will have to phone the bank manager?")
    else:
      self.__balance = self.__balance - value
      print("You now have: ",self.__balance)

# SavingsAccount
class SavingsAccount(BankAccount):
  def __init__(self,account_name="SavingsAccount",balance=1500):
    self.__account_name = account_name
    self.__balance = balance
    BankAccount.__init__(self)

  def deposit(self,value):
    print('\nSavingsAccount')
    self.__balance = self.__balance + (value*1.03)
    print('You now have: ',self.__balance)

current = CurrentAccount()
savings = SavingsAccount()


while True:
  print("1. Current Account")
  print("2. Savings Account")
  menu_option = int(input())

  if menu_option == 1:
    print("1. Deposit funds")
    print("2. Withdraw funds")
    submenu_option = int(input())
    
    if submenu_option == 1:
      value = int(input('How much would you like to deposit?'))
      current.deposit(value)
    elif submenu_option == 2:
      value = int(input('How much would you like to withdraw?'))
      current.withdraw(value)
    else:
      print('Wrong menu choice!')

  elif menu_option == 2:
    print("1. Deposit funds")
    print("2. Withdraw funds")
    submenu_option = int(input())

    if submenu_option == 1:
      value = int(input('How much would you like to deposit?'))
      savings.deposit(value)
    elif submenu_option == 2:
      value = int(input('How much would you like to withdraw?'))
      savings.withdraw(value)
    else:
      print('Wrong menu choice!')
     
  else:
      print('Wrong menu choice!')


  