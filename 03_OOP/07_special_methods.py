class Employee:
  raise_amt = 1.04  # 4%

  def __init__(self,first,last,pay):
    self.first = first
    self.last = last
    self.email = f'{first}.{last}@gmail.com'
    self.pay = pay
  
  # regular methods of class takes an instance as first argument,which is: self
  def fullname(self):         # regular method   
    return f'{self.first} {self.last}'
  
  def appy_raise(self):       # regular method
    self.pay = int(self.pay * self.raise_amt)


  # Special Methods  
  # return a string that you can use to re-create the object  or an instance of this class
  def __repr__(self):
    return f"Employee('{self.first}','{self.last}','{self.pay}')"

  # More for Readability for the end user
  # some time arbitrary value
  def __str__(self):
    return f'{self.fullname()}-{self.email}'

  
  def __len__(self):
    return len(self.fullname())



employee_1 = Employee('Jacob','Sima',50000)
# print(employee_1)   # <__main__.Employee object at 0x0051DDD0>

print('\n# Special Method most used,repr and str')
print(repr(employee_1))    # Employee('Jacob','Sima','50000')
print(str(employee_1))     # Jacob Sima-Jacob.Sima@gmail.com

print('\n# Others Special Method')
print(len(employee_1))   # 10
