class Employee:
  def __init__(self,first,last):
    self.first = first
    self.last = last
    # self.email = f'{first}.{last}@gmail.com'

  # Define this as a function but access it as an property 
  # as decorator property was added on top of it
  @property
  def email(self):         # regular method   
    return f'{self.first}.{self.last}@gmail.com'

  
  # this was a method but convert into a property  to be able to set class property
  # using the setter function below with the same name
  @property
  def fullname(self):           
    return f'{self.first} {self.last}'
  
  @fullname.setter
  def fullname(self,name):
    first,last = name.split(' ')
    self.first = first
    self.last = last


  @fullname.deleter
  def fullname(self):
    print('delete Name!')
    self.first = None
    self.last = None
   


employee_1 = Employee('Jacob','Sima')
employee_1.first = 'Jim'
employee_1.fullname = 'cooly bally'


print(employee_1.first)
print(employee_1.email)
print(employee_1.fullname)

del employee_1.fullname






# Example
mylist = [1,2,3,4]
print(len(mylist))
print(mylist)    # get [1, 2, 3, 4]

class Sample():
  pass
mysample = Sample()
print(mysample)           # <__main__.Sample object at 0x01D4F7B0> only the location
#print(len(mysample))     # TypeError: object of type 'Sample' has no len()

print(' ')
print(' ')
print(' ')

# Special methods will help us to do this for a class
class Book():
  def __init__(self,title,author,pages):
    self.title = title
    self.author = author
    self.pages = pages
  
  # Special methods for string or printing
  def __str__(self):
    return f'{self.title} by {self.author}'

  # Special methods for length
  def __len__(self):
    return self.pages

  # run something on delete
  def __del__(self):
    print('A Book object has been deleted')

  # Special methods 
  
  


